#Project
Game Component Integration Assignment 4

#Team Members
Benjamin Taylor
Jorge Amengol
Euclides Araujo
Thomas Churchill

#Due Date
January 3rd 2018

#How to Run
Open project in Visual Studio 2017, and run in Debug/Release x86

To Run GameService alongside the game, either build the GameService and run the .exe in it's debug folder or
setup the solution properties to run multiple projects with the GameService starting first

#Controls
Change Ship acceleration with WASD
Shoot bullets with ARROW KEYS
Retrieve information from the SQLite database with 3 depending on current Id (Displayed in window titlebar)
Insert/Update information in the SQLite database with 4 based on current Id (Displayed in window titlebar)
Increase/Decrease Id with 1 and 2


#Notes
- The db file for SQLite is located in the VirusAttack project directory.
- SQLite logic can be found in SQLiteUtilities
- The GameService project contains the server side of the Thrift/MySQL portion of the project
- SQLServerManager contains the client side of this portion
- The client side was not completed
- FMOD implementation is found in the AudioManager
- Lua implementation is found in the cLuaBrain
- Lua script events can be found when a shot is fired (ARROW KEYS in the key handler)
  and in the physics step when a virus is destroyed
 
#Github / Bitbucket 
https://bitbucket.org/b_taylor36/virusattack.git