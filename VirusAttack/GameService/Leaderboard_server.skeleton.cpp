// This autogenerated skeleton file illustrates how to build a server.
// You should copy it to another filename to avoid overwriting it.

#include "Leaderboard.h"
#include <thrift/protocol/TBinaryProtocol.h>
#include <thrift/server/TSimpleServer.h>
#include <thrift/transport/TServerSocket.h>
#include <thrift/transport/TBufferTransports.h>
#include"DatabaseHelper.h"
using namespace ::apache::thrift;
using namespace ::apache::thrift::protocol;
using namespace ::apache::thrift::transport;
using namespace ::apache::thrift::server;

using boost::shared_ptr;

class LeaderboardHandler : virtual public LeaderboardIf {
 public:
	 DatabaseHelper dbHelper;

  LeaderboardHandler() {
    // Your initialization goes here
	  if (!dbHelper.ConnectToDatabase("127.0.0.1:3306", "root", "password", "highscore"))
	  {
		 std:: cout << "Failed to connect to the database!" << std::endl;
		  
	  }

  }

  void setHighScore(const int32_t playerId, const int32_t highScore) {
    // Your implementation goes here
	  std::string sqlStatement = "UPDATE highscores SET score = ";// +  highScore + "WHERE id = " + playerId;// WHERE id = %d;";
	  sqlStatement += highScore;
	  sqlStatement += " WHERE id = ";
	  sqlStatement += playerId;
	  sqlStatement += ";";
	  int numUpdates = dbHelper.ExecuteUpdate(sqlStatement);
    printf("setHighScore\n");
  }

  void getTop20(std::map<int32_t, int32_t> & _return) {
	  sql::ResultSet* result = dbHelper.ExecuteQuery("SELECT * FROM highscores ORDER BY score DESC LIMIT 20;");

	  if (result->rowsCount() > 0)
	  {
		  while (result->next())
		  {
			  int id = result->getInt(0);
			  int score = result->getInt(1);

			  std::cout << "Id: " << id << std::endl;
			  std::cout << "Score: " << score << std::endl;
		  }
	  }
    printf("getTop20\n");
  }

};

int main(int argc, char **argv) {
	WSADATA wsa_data;
	int result = WSAStartup(MAKEWORD(2, 2), &wsa_data);

  int port = 9090;
  shared_ptr<LeaderboardHandler> handler(new LeaderboardHandler());
  shared_ptr<TProcessor> processor(new LeaderboardProcessor(handler));
  shared_ptr<TServerTransport> serverTransport(new TServerSocket(port));
  shared_ptr<TTransportFactory> transportFactory(new TBufferedTransportFactory());
  shared_ptr<TProtocolFactory> protocolFactory(new TBinaryProtocolFactory());

  TSimpleServer server(processor, serverTransport, transportFactory, protocolFactory);
  server.serve();
  return 0;
}

