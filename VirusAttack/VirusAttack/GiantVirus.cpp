#include"GiantVirus.h"
#include"GameObject.h"
//Constructor
GiantVirus::GiantVirus()
{
	//TODO Set some defaults
	return;
}

//Destructor
GiantVirus::~GiantVirus()
{
	//Delete anything neccessary
	return;

}

//Pimpl implementation
class GiantVirus::ImplementationData
{
public:
	int internalData = 0;

};

//Method Implementations


void GiantVirus::FindTarget()
{
	return;
}

void GiantVirus::SpawnChild()
{
	return;
}

void GiantVirus::SpawnObjectOnDeath()
{
	return;
}

void GiantVirus::SetPosition(glm::vec3 newPos)
{
	this->pMesh->position = newPos;
	return;
}

void GiantVirus::SetVelocity(glm::vec3 newVel)
{
	this->pMesh->vel = newVel;
	return;
}