#include "GameObject.h"

//Constructor
GameObject::GameObject()
{
	this->scale = 1.0f;
	this->position = glm::vec3(0.0f);
	//this->orientation2 = glm::vec3(0.0f);
	//this->orientation = glm::vec3(0.0f);
	this->vel = glm::vec3(0.0f);
	this->accel = glm::vec3(0.0f);
	this->isWireFrame = false;
	this->rotation = glm::quat(glm::vec3(0.0f));
	this->diffuseColour = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);

	this->IsUpdatedInPhysics = true; 
	this->radius = 0.0f;

	this->typeOfObject = eTypeOfObject::UNKNOWN;


	return;
}

//Destructor
GameObject::~GameObject()
{
	return;
}
