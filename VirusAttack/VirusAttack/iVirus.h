#ifndef _iVirus_HG_
#define _iVirus_HG_

#include <glm/glm.hpp>

// Header Definition of iVirus
// Name: iVirus
// Purpose: The interface that all virus classes will inherit from
class iVirus
{
public:
	//Destructor
	virtual ~iVirus() {};		

	/*
	* Name: FindTarget
	* Purpose: To find the ship object relative to the current position of the virus object
	* Accepts: Nothing
	* Returns: Nothing
	*/
	virtual void FindTarget(void) = 0;

	/*
	* Name: SpawnChild
	* Purpose: Create a child virus based on the parent virus' properties
	* Accepts: Nothing
	* Returns: Nothing
	*/
	virtual void SpawnChild(void) = 0;

	/*
	* Name: SpawnObjectOnDeath
	* Purpose: Create an object based on the parent virus' properties
	* Accepts: Nothing
	* Returns: Nothing
	*/
	virtual void SpawnObjectOnDeath(void) = 0;


	/*
	* Name: SetPosition
	* Purpose: sets the position of the virus from the position passed in
	* Accepts: a vec3 for the new position
	* Returns: Nothing
	*/
	virtual void SetPosition(glm::vec3 newPos) = 0;

	/*
	* Name: SetVelocity
	* Purpose: sets the velocity of the virus from the velocity passed in
	* Accepts: a vec3 for the new velocity
	* Returns: Nothing
	*/
	virtual void SetVelocity(glm::vec3 newVel) = 0;
};

#endif 
