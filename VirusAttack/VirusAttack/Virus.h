#ifndef _Virus_HG_
#define _Virus_HG_

#include "iVirus.h"

class GameObject;	// forward declare


// Header Definition of Virus
// Name: Virus
// Purpose: The representation of what a "Virus" will be during gameplay in terms of behaviour and properties
//			Contains inherited methods from the iVirus interface that influences what it can do
//inherit interface
class Virus : public iVirus
{
public:
	Virus();
	virtual ~Virus();

	//Inherited methods
	virtual void FindTarget(void);
	virtual void SpawnChild(void);
	virtual void SpawnObjectOnDeath(void);

	virtual void SetPosition(glm::vec3 newPos);
	virtual void SetVelocity(glm::vec3 newVel);

	GameObject* pMesh;

private:	//Pimpl
	class ImplementationData;
	ImplementationData* pimpl;
};

#endif 
