#ifndef _VAOMeshManager_HG_
#define _VAOMeshManager_HG_


#include <string>
#include <map>

// Forward declare
class Mesh;

// struct for sVAOInfo
// Name: sVAOInfo
// Purpose: Will be used to store information for meshes that have been loaded.
struct sVAOInfo
{
	sVAOInfo() : VAO_ID(0), shaderID(-1),
		vertex_buffer_ID(0), index_buffer_ID(0),
		numberOfVertices(0), numberOfTriangles(0),
		numberOfIndices(0),
		scaleForUnitBBox(1.0f)
	{
	}
	std::string friendlyName;
	// OpenGL rendering specific stuff
	unsigned int VAO_ID;	
	unsigned int numberOfVertices;
	unsigned int numberOfTriangles;
	unsigned int numberOfIndices;		


	int shaderID;			
	unsigned int vertex_buffer_ID;		
	unsigned int index_buffer_ID;

	float scaleForUnitBBox;
};

// Header Definition of VAOMeshManager
// Name: VAOMeshManager
// Purpose: takes a Mesh and loads it into the various buffers, 
//          will also take a mesh "name" and returning the desired VAO info 
class VAOMeshManager
{
public:

	

	/*
	* Name: getInstance
	* Purpose: to get the instance of the VAOMeshManager Singleton
	* Accepts: Nothing
	* Returns: an Instance of the VAOMeshManager
	*/
	static VAOMeshManager& getInstance();

	//Destructor

	~VAOMeshManager();

	/*
	* Name: loadMeshIntoVAO
	* Purpose: Take name from mesh and store it for lookup for later rendering
	* Accepts: a Mesh object which information is being taken, and the Shader ID of the shader that's being used
	* Returns: true if loading the Mesh succeeds or false if it fails
	*/
	bool loadMeshIntoVAO(Mesh &theMesh, int shaderID);

	/*
	* Name: lookupVAOFromName
	* Purpose: Retrieve the rendering information for mesh by looking it by Mesh name
	* Accepts: the name of the mesh whose information we're trying to retrieve, 
	*          an sVAOInfo by reference that will be pushed with the information found during the mesh lookup
	* Returns: true if loading the lookup succeeds or false if it fails
	*/
	bool lookupVAOFromName(std::string name, sVAOInfo &theVAOInfo);

	std::map<int, std::string> mapNumberToName;

private:
	//Map that contains the friendly mesh name and the rendering info for that mesh
	std::map< std::string, sVAOInfo > m_mapNameToVAO;
	//Singleton
	
	//Constructor
	VAOMeshManager()
	{
		//VAOMeshManager::instance = NULL;
		return;
	}
	
};


#endif
