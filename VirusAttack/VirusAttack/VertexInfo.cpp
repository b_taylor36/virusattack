#include "VertexInfo.h"

// Constructor
VertexInfo::VertexInfo()
{
	this->pos = glm::vec3(0.0f, 0.0f, 0.0f);
	this->col = glm::vec3(0.0f, 0.0f, 0.0f);
	this->npos = glm::vec3(0.0f, 0.0f, 0.0f);
	return;
}

// Destructor
VertexInfo::~VertexInfo()
{
	return;
}
