#include"VirusBomb.h"
#include<iostream>

//constructor
VirusBomb::VirusBomb()
{
	VirusBomb::explosionDamage = rand() % 5 + 1;
	VirusBomb::explosionRadius = rand() % 20 + 1;
	return;
}

//destructor
VirusBomb::~VirusBomb()
{
	return;
}

void VirusBomb::DetonateBomb()
{
	std::cout << "Bomb detonated with a radius of " << VirusBomb::explosionRadius << ", while dealing " 
			  << VirusBomb::explosionDamage << " damage..." << std::endl;
}