#include"FastVirus.h"
#include"GameObject.h"

//Constructor
FastVirus::FastVirus()
{
	//TODO Set some defaults
	return;
}

//Destructor
FastVirus::~FastVirus()
{
	//Delete anything neccessary
	return;

}

//Pimpl implementation
class FastVirus::ImplementationData
{
public:
	int internalData = 0;

};

//Method Implementations


void FastVirus::FindTarget()
{
	return;
}

void FastVirus::SpawnChild()
{
	return;
}

void FastVirus::SpawnObjectOnDeath()
{
	return;
}

void FastVirus::SetPosition(glm::vec3 newPos)
{
	this->pMesh->position = newPos;
	return;
}

void FastVirus::SetVelocity(glm::vec3 newVel)
{
	this->pMesh->vel = newVel;
	return;
}