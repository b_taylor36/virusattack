#ifndef _SQLiteUtilities_HG_
#define _SQLiteUtilities_HG_

#include "sqlite3.h"

#include <string>
#include <iostream>



int CheckSQLiteErrors(int result);
bool OpenSQLiteDB(const std::string& filename);
void CloseSQLiteDB();
int SQLiteCallback(void *v, int argc, char** argv, char** azColName);
void ExecuteSQLite(char* sql);
void AddHighScore(int id, int score);
void GetHighScores();
void GetHighScoreId(int id);
void SetHighScore(int id, int score);
void DeleteHighScore(int id);
void RunSQLiteExample();

#endif