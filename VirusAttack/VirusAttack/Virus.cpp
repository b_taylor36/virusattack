#include"Virus.h"
#include"GameObject.h"
//Constructor
Virus::Virus()
{
	//TODO Set some defaults
	return;
}

//Destructor
Virus::~Virus()
{
	//Delete anything neccessary
	return;

}

//Pimpl implementation
class Virus::ImplementationData{
public:
	int internalData = 0;
	float internalData2 = 0.0f;
};


//Method Implementations


void Virus::FindTarget()
{
	return;
}

void Virus::SpawnChild()
{
	return;
}

void Virus::SpawnObjectOnDeath()
{
	return;
}

void Virus::SetPosition(glm::vec3 newPos)
{
	this->pMesh->position = newPos;
	return;
}

void Virus::SetVelocity(glm::vec3 newVel)
{
	this->pMesh->vel = newVel;
	return;
}

