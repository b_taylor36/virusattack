#ifndef _GameObject_HG_
#define _GameObject_HG_

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include<glm/gtc/quaternion.hpp>
#include<glm/gtx/quaternion.hpp>
#include <string>


enum eTypeOfObject
{	
	SPHERE = 0,		
	PLANE = 1,		
	CAPSULE = 2,    
	AABB = 3,		
	OBB = 4,		
	UNKNOWN = 99	
};

// Header Definition of GameObject
// Name: GameObject
// Purpose: The representation of what will be displayed and manipulated during the game
//			Contains information for position, orientation, colour and physics of the object
class GameObject
{
public:
	GameObject();		// constructor
	~GameObject();		// destructor
	glm::vec3 position;
	/*glm::vec3 orientation;
	glm::vec3 orientation2;*/
	glm::quat rotation;
	float scale;
	bool isWireFrame;
	
	//physics variables
	glm::vec3 vel;
	glm::vec3 accel;
	bool IsUpdatedInPhysics;

	eTypeOfObject typeOfObject;
	float radius;
	glm::vec4 diffuseColour;	
	//Name of mesh to draw
	std::string meshName;		

	
};

#endif
