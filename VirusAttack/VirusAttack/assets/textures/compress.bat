@ECHO OFF
for %%a in (*.png) DO ( 
 IF NOT EXIST %%a.pvr (
 ECHO Compressing with pngquant...
 pngquant.exe --quality=65-80 %%a --output temp.png 
 ECHO Compressing with optipng...
 optipng temp.png
 ECHO Compressing and Converting with PVRTexTool...
 PVRTexToolCLI.exe -i temp.png -m -f pvrtc2_4,ubn,lrgb -o %%a.pvr
 ECHO Deleting Temp file
 DEL temp.png
) ELSE (
  ECHO %%a.pvr already exists
)
) 