#ifndef _cLightManager_HG_
#define _cLightManager_HG_

#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <vector>

// Header Definition of Light
// Name: Light
// Purpose: Store relevent information regarding light objects
class Light
{
public:
	//Constructor/Destructor
	Light();
	~Light();
	glm::vec3 position;
	glm::vec3 diffuse;		
	glm::vec3 ambient;
	glm::vec4 specular;		
	glm::vec3 attenuation;	
	glm::vec3 direction;
	glm::vec4 typeParams;	
							
	int shaderlocID_position;
	int shaderlocID_diffuse;
	int shaderlocID_ambient;
	int shaderlocID_specular;
	int shaderlocID_attenuation;
	int shaderlocID_direction;
	int shaderlocID_typeParams;
};

// Header Definition of LightManager
// Name: LightManager
// Purpose: has a container of Light Objects and various functions to manipulate lights
class LightManager
{
public:
	//Constructor/Destructor
	LightManager();
	~LightManager();

	/*
	* Name: CreateLights
	* Purpose: Generates light objects and pushes them into the vector of lights
	* Accepts: an int for the number of lights to be created, a bool for Keep older values of previous lights
	* Returns: Nothing
	*/
	void CreateLights(int numberOfLights, bool bKeepOldValues = true);
	/*
	* Name: LoadShaderUniformLocations
	* Purpose: Receives the uniform locations of variables from a specific shader ID
	* Accepts: an int for the Shader ID
	* Returns: Nothing
	*/
	void LoadShaderUniformLocations(int shaderID);
	/*
	* Name: CopyLightInformationToCurrentShader
	* Purpose: Takes the information from each light in vecLights and copies it into the current Shader
	* Accepts: Nothing
	* Returns: Nothing
	*/
	void CopyLightInformationToCurrentShader(void);

	std::vector<Light> vecLights;
};

#endif
