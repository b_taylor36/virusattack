#ifndef _ShaderManager_HG_
#define _ShaderManager_HG_

#include <string>
#include <vector>
#include <map>

// Header Definition of ShaderManager
// Name: ShaderManager
// Purpose: Manages which shaders will be used as well as compiling and linking the shaders and error handling
//			Also has 2 Nested Classes for implementation details
class ShaderManager
{
public:
	// Header Definition of Shader
	// Name: Shader
	// Purpose: Holds information regarding the type of shader being used and the Shader's ID
	class Shader
	{
	public:
		//Constructor/Destructor
		Shader();
		~Shader();
		enum eShaderType
		{
			VERTEX_SHADER,
			FRAGMENT_SHADER,
			UNKNOWN
		};
		eShaderType shaderType;
		/*
		* Name: getShaderTypeString
		* Purpose: Converts the enumtype of the Shader in the form of a string
		* Accepts: Nothing
		* Returns: The String equivalent of the eShaderType of the Shader
		*/
		std::string getShaderTypeString(void);

		unsigned int ID;	// or "name" from OpenGL
		std::vector<std::string> vecSource;
		bool bSourceIsMultiLine;
		std::string fileName;
	};

	// Header Definition of ShaderProgram
	// Name: ShaderProgram
	// Purpose: Holds information for matching a shader ID and the shader's friendly name
	class ShaderProgram
	{
	public:
		ShaderProgram() : ID(0) {};
		~ShaderProgram() {};
		unsigned int ID;	// ID from OpenGL (calls it a "name")
		std::string friendlyName;	// We give it this name
	};

	//Constructor/Destructor
	ShaderManager();
	~ShaderManager();

	/*
	* Name: useShaderProgram
	* Purpose: uses Shader based on which shader ID is given
	* Accepts: a Shader ID
	* Returns: true if the the shader is being used
	*/
	bool useShaderProgram(unsigned int ID);
	/*
	* Name: useShaderProgram
	* Purpose: uses Shader based on which shader name is given
	* Accepts: a Shader friendly name
	* Returns: true if the the shader is being used
	*/
	bool useShaderProgram(std::string friendlyName);
	/*
	* Name: createProgramFromFile
	* Purpose: creates a shader program using a vertex and fragment shader
	* Accepts: a name to reference the new program, a vertex shader
	* Returns: true if the the program was created
	*/
	bool createProgramFromFile(std::string friendlyName,
		Shader &vertexShad,
		Shader &fragShader);
	/*
	* Name: setBasePath
	* Purpose: changes the base path for looking for shaders
	* Accepts: a string of the basepath
	* Returns: Nothing
	*/
	void setBasePath(std::string basepath);
	/*
	* Name: getIDFromFriendlyName
	* Purpose: to obtain a shader ID from a shader friendly name
	* Accepts: a string of the friendly shader name
	* Returns: a Shader ID
	*/
	unsigned int getIDFromFriendlyName(std::string friendlyName);

	/*
	* Name: getLastError
	* Purpose: return the last possible shader error while also clearing the error
	* Accepts: Nothing
	* Returns: a string of the last error
	*/
	std::string getLastError(void);

private:
	/*
	* Name: m_loadSourceFromFile
	* Purpose: Load Shader file information
	* Accepts: a Shader variable by reference
	* Returns: true if the file was read, false if the file failed to read
	*/
	bool m_loadSourceFromFile(Shader &shader);
	std::string m_basepath;

	/*
	* Name: m_compileShaderFromSource
	* Purpose: Compiles the shader using information from a shader source
	* Accepts: a Shader variable by reference, a string by reference for the error
	* Returns: true if the shader was compiled
	*/
	bool m_compileShaderFromSource(Shader &shader, std::string &error);
	/*
	* Name: m_wasThereACompileError
	* Purpose: Checks if there was an error during compiling the shaders
	* Accepts: a Shader ID, a string by reference for the error
	* Returns: false if the shader was compiled without a compile error
	*/
	bool m_wasThereACompileError(unsigned int shaderID, std::string &errorText);
	/*
	* Name: m_wasThereALinkError
	* Purpose: Checks if there was an error during the linking of the shaders
	* Accepts: a program ID, a string by reference for the error
	* Returns: false if the shader was compiled without a linker error
	*/
	bool m_wasThereALinkError(unsigned int progID, std::string &errorText);

	std::string m_lastError;

	std::map< unsigned int /*ID*/, ShaderProgram > m_ID_to_Shader;
	std::map< std::string /*name*/, unsigned int /*ID*/ > m_name_to_ID;
};

#endif