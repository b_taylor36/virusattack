#include "LightManager.h"
#include <vector>
#include <sstream>
#include <glad/glad.h>
#include <GLFW/glfw3.h>

//Constructor
Light::Light()
{
	this->position = glm::vec3(0.0f);

	this->diffuse = glm::vec3(0.0f, 0.0f, 0.0f);
	this->ambient = glm::vec3(0.2f, 0.2f, 0.2f);
	this->specular = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
	this->attenuation = glm::vec3(0.0f);
	this->attenuation.x = 0.0f;	// Constant atten
	this->attenuation.y = 1.0f;	// Linear
	this->attenuation.z = 0.0f;	// Quadratic

	this->direction = glm::vec3(0.0f);

	// x = type, y = distance cut-off, z angle1, w = angle2
	this->typeParams = glm::vec4(0.0f);
	// Set distance to infinity
	// (i.e. at this point, the light won't be calculated)
	this->typeParams.y = 1000000.0f;		// Huge number

	this->shaderlocID_position = -1;
	this->shaderlocID_diffuse = -1;
	this->shaderlocID_ambient = -1;
	this->shaderlocID_specular = -1;
	this->shaderlocID_attenuation = -1;
	this->shaderlocID_direction = -1;
	this->shaderlocID_typeParams = -1;

	return;
}

//Destructor
Light::~Light()
{
	return;
}

//Constructor
LightManager::LightManager()
{

	return;
}

//Destructor
LightManager::~LightManager()
{

	return;
}

/*
* Name: CreateLights
* Purpose: Generates light objects and pushes them into the vector of lights
* Accepts: an int for the number of lights to be created, a bool for Keep older values of previous lights
* Returns: Nothing
*/
void LightManager::CreateLights(int numberOfLights, bool bKeepOldValues)
{
	int howManyLightsToAdd = numberOfLights - this->vecLights.size();
	howManyLightsToAdd = abs(howManyLightsToAdd);
	// Resize the vector
	this->vecLights.resize(howManyLightsToAdd, Light());

	Light tempLight;
	for (int index = 0; index != this->vecLights.size(); index++)
	{
		this->vecLights[index] = tempLight;
	}

	return;
}

/*
* Name: genUniName
* Purpose: Generates a name for the light for when it is copied into the shader
* Accepts: an int for the light index, a string for the parameters of the light
* Returns: a string of the name that will be copied into the shader
*/
std::string genUniName(int lightIndex, std::string paramName)
{
	std::stringstream ssUniName;
	ssUniName << "myLight[" << lightIndex << "]." << paramName;
	return ssUniName.str();
}

/*
* Name: LoadShaderUniformLocations
* Purpose: Receives the uniform locations of variables from a specific shader ID
* Accepts: an int for the Shader ID
* Returns: Nothing
*/
void LightManager::LoadShaderUniformLocations(int shaderID)
{

	for (int index = 0; index != this->vecLights.size(); index++)
	{
		//"myLight[0].position"
		this->vecLights[index].shaderlocID_position
			= glGetUniformLocation(shaderID, genUniName(index, "position").c_str());

		this->vecLights[index].shaderlocID_diffuse = glGetUniformLocation(shaderID, genUniName(index, "diffuse").c_str());
		this->vecLights[index].shaderlocID_ambient = glGetUniformLocation(shaderID, genUniName(index, "ambient").c_str());
		this->vecLights[index].shaderlocID_specular = glGetUniformLocation(shaderID, genUniName(index, "specular").c_str());
		this->vecLights[index].shaderlocID_attenuation = glGetUniformLocation(shaderID, genUniName(index, "attenuation").c_str());
		this->vecLights[index].shaderlocID_direction = glGetUniformLocation(shaderID, genUniName(index, "direction").c_str());
		this->vecLights[index].shaderlocID_typeParams = glGetUniformLocation(shaderID, genUniName(index, "typeParams").c_str());
	}
	return;
}
/*
* Name: CopyLightInformationToCurrentShader
* Purpose: Takes the information from each light in vecLights and copies it into the current Shader
* Accepts: Nothing
* Returns: Nothing
*/
void LightManager::CopyLightInformationToCurrentShader(void)
{
	for (int index = 0; index != this->vecLights.size(); index++)
	{
		Light& pCurLight = this->vecLights[index];

		glUniform4f(pCurLight.shaderlocID_position,
			pCurLight.position.x,
			pCurLight.position.y,
			pCurLight.position.z,
			1.0f);

		glUniform4f(pCurLight.shaderlocID_diffuse,
			pCurLight.diffuse.r,
			pCurLight.diffuse.g,
			pCurLight.diffuse.b,
			1.0f);

		glUniform4f(pCurLight.shaderlocID_ambient,
			pCurLight.ambient.r,
			pCurLight.ambient.g,
			pCurLight.ambient.b,
			1.0f);

		glUniform4f(pCurLight.shaderlocID_specular,
			pCurLight.specular.r,
			pCurLight.specular.g,
			pCurLight.specular.b,
			pCurLight.specular.w);

		glUniform4f(pCurLight.shaderlocID_attenuation,
			pCurLight.attenuation.x,
			pCurLight.attenuation.y,
			pCurLight.attenuation.z,
			1.0f);

		glUniform4f(pCurLight.shaderlocID_direction,
			pCurLight.direction.x,
			pCurLight.direction.y,
			pCurLight.direction.z,
			1.0f);


		glUniform4f(pCurLight.shaderlocID_typeParams,
			pCurLight.typeParams.x,
			pCurLight.typeParams.y,
			pCurLight.typeParams.z,
			pCurLight.typeParams.w);
	}

	return;
}
