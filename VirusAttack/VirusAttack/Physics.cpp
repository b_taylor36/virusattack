#include "Physics.h"
#include "GameObject.h"
#include "PhysTriangle.h"
#include"Mesh.h"


/*
* Name: PenetrationTestSphereSphere
* Purpose: To check if 2 Spheres have penetrated
* Accepts: 2 game object pointers
* Returns: true if the spheres are penetrating
*/
bool PenetrationTestSphereSphere(GameObject* pA, GameObject* pB)
{
	// If the distance between the two sphere is LT the sum or the radii,
	//	they are touching or penetrating

	float totalRadii = pA->radius + pB->radius;

	// The Pythagorean distance 
	float distance = glm::distance(pA->position, pB->position);

	if (distance <= totalRadii)
	{
		return true;
	}

	return false;
}

/*
* Name: PerformSphereSphereCollision
* Purpose: to perform a response to a sphere-sphere collision
* Accepts: 2 game object pointers
* Returns: Nothing
*/
void PerformSphereSphereCollision(GameObject* pA, GameObject* pB)
{
	glm::vec3 newVelocityA;
	glm::vec3 newVelocityB;

	newVelocityA = pA->vel;
	newVelocityA += CalculateVelocity(pB->vel, (pB->position - pA->position));
	newVelocityA -= CalculateVelocity(pA->vel, (pA->position - pB->position));

	newVelocityB = pB->vel;
	newVelocityB += CalculateVelocity(pA->vel, (pB->position - pA->position));
	newVelocityB -= CalculateVelocity(pB->vel, (pA->position - pB->position));

	pA->vel = newVelocityA;
	pB->vel = newVelocityB;
}


/*
* Name: CalculateVelocity
* Purpose: calculate the return velocity of an object during collision
* Accepts: a vec3 containing velocity, and a vec3 containing the difference in position between the two objects
* Returns: a vec3 containing the return velocity
*/
glm::vec3 CalculateVelocity(glm::vec3 otherObjectVelocity, glm::vec3 positionDiff)
{
	glm::vec3 returnVelocity;

	returnVelocity = positionDiff * (glm::dot(otherObjectVelocity, positionDiff) / glm::dot(positionDiff, positionDiff));

	return returnVelocity;
}

/*
* Name: FindClosestPointsOnTerrainTriangles
* Purpose: To check for the closest point between a gameobject and a terrains' triangles
* Accepts: a game object pointer, a Mesh to test against the game object, a vec3 point by reference and a Phystriangle by reference
* Returns: true if the object is penetrating one of the triangles
*/
bool FindClosestPointsOnTerrainTriangles(GameObject* pO, Mesh theMesh, glm::vec3 &point, PhysTriangle &triangle)
{
	// std::vector< cPhysTriangle > vecPhysTris;
	glm::vec3 thePoint = pO->position;
	int numberOfTriangles = theMesh.vecPhysTris.size();

	for (int triIndex = 0; triIndex != numberOfTriangles; triIndex++)
	{
		// Get reference object for current triangle
		// (so the line below isn't huge long...)
		PhysTriangle& curTriangle = theMesh.vecPhysTris[triIndex];

		glm::vec3 theClosestPoint = curTriangle.ClosestPtPointTriangle(thePoint);
		float distance = glm::distance(pO->position, theClosestPoint);

		if (distance <= pO->radius)
		{
			triangle = curTriangle;
			point = theClosestPoint;
			return true;
		}
		
	}

	return false;
}

/*
* Name: PerformSphereTriangleCollision
* Purpose: to perform a response to a sphere-triangle collision
* Accepts: a game object pointer, a vec3 point, a PhysTriangle, a bool to determine is the object is a OBB
* Returns: Nothing
*/
void PerformSphereTriangleCollision(GameObject* pA, glm::vec3 point, PhysTriangle triangle, bool isOBB)
{

	glm::vec3 difference = (pA->position - point);
	float distance = glm::distance(pA->position, point);

	glm::vec3 sphereOffset = difference * (pA->radius - distance) / distance;

	glm::vec3 normalizedTri = glm::normalize(glm::cross(triangle.vertex[2] - triangle.vertex[0], triangle.vertex[1] - triangle.vertex[0]));

	glm::vec3 reflected = glm::vec3(2.0f) * normalizedTri * (normalizedTri * pA->vel);

	pA->position += sphereOffset;

	//Extra calculation for slanted objects (OBB) so that spheres don't get stuck
	if (isOBB)
	{
		if (reflected.x < 0.0f  && reflected.x > -1.0f)
		{
			reflected.x = -pA->vel.x;
		}
		if (reflected.y < 0.0f  && reflected.y > -1.0f)
		{
			reflected.y = -pA->vel.y;
		}
		if (reflected.z < 0.0f  && reflected.z > -1.0f)
		{
			reflected.z = -pA->vel.z;
		}
	}

	pA->vel -= reflected;

}
