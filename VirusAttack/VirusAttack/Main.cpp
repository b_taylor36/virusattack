#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <iostream>			
#include <glm/glm.hpp> 
#include <glm/vec4.hpp> 
#include <glm/mat4x4.hpp> 
#include <glm/gtc/matrix_transform.hpp> 
#include <glm/gtc/type_ptr.hpp> 
#include<rapidjson\document.h>
#include <rapidjson\filereadstream.h>
#include <stdio.h>
#include <stdlib.h>
#include<ctime>
#include <fstream>
#include <sstream>		
#include <cstdio>
#include <string>
#include <vector>		
#include "ModelUtilities.h"
#include "Mesh.h"
#include "ShaderManager.h" 
#include "GameObject.h"
#include "VAOMeshManager.h"
#include "Physics.h"	
#include "LightManager.h"
#include"VirusFactory.h"
#include"SQLiteUtilities.h"
#include "AudioManager.h"
#include"cLuaBrain.h"
// Other uniforms:
GLint uniLoc_materialDiffuse = -1;
GLint uniLoc_materialAmbient = -1;
GLint uniLoc_ambientToDiffuseRatio = -1; 	
GLint uniLoc_materialSpecular = -1;  	 
GLint uniLoc_eyePosition = -1;	
GLint uniLoc_mModel = -1;
GLint uniLoc_mView = -1;
GLint uniLoc_mProjection = -1;

//Global variables
glm::vec3 g_CameraPos = glm::vec3(0.0f, 320.0f, 30.0f);
glm::vec3 g_CameraTarget = glm::vec3(0.0f, 0.0f, 0.0f);
unsigned int g_UsedBullet = 2;
unsigned int g_CurrentLightIndex = 0;
unsigned int g_CurrentObjectIndex = 1;
unsigned int g_DbID = 1;
unsigned int g_Score = 0;
std::vector<glm::vec3> g_LightColourStorage;
std::vector< GameObject* >  g_vecGameObjects;
std::map<std::string, Mesh> g_Meshes;
GameObject* g_pTheDebugSphere;
VirusFactory* g_VirusFactory;
sqlite3* db;

const int MAX_SQL_LENGTH = 200;

//Managers
VAOMeshManager		g_pVAOManager = VAOMeshManager::getInstance();												
ShaderManager*		g_pShaderManager = 0;		
LightManager*		g_pLightManager = 0;
AudioManager*		g_pAudioManager = 0;

cLuaBrain*			g_LuaBrain = 0;

//Foward declaring functions
void DrawObject(GameObject* pGO);
void PhysicsStep(double deltaTime);
void CreateGameObject(glm::vec3 pos, float scale, glm::vec4 diffuse
	, std::string meshName, eTypeOfObject typeOfObject, bool isUpdatedPhysics, float radius, glm::vec3 velocity);



//Key Handler
static void error_callback(int error, const char* description)
{
	fprintf(stderr, "Error: %s\n", description);
}

static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GLFW_TRUE);


	const float CAMERASPEED = 1.25f;
	switch (key)
	{
	case GLFW_KEY_A:		// Left
		if (action == GLFW_PRESS)
			::g_vecGameObjects[g_CurrentObjectIndex]->accel.x = -5.0f;
		else if (action == GLFW_RELEASE)
			::g_vecGameObjects[g_CurrentObjectIndex]->accel.x = 0.0f;
		break;
	case GLFW_KEY_D:		// Right
		if (action == GLFW_PRESS)
			::g_vecGameObjects[g_CurrentObjectIndex]->accel.x = 5.0f;
		else if (action == GLFW_RELEASE)
			::g_vecGameObjects[g_CurrentObjectIndex]->accel.x = 0.0f;
		break;
	case GLFW_KEY_S:		// Forward (along z)
		if (action == GLFW_PRESS)
			::g_vecGameObjects[g_CurrentObjectIndex]->accel.z = 5.0f;
		else if (action == GLFW_RELEASE)
			::g_vecGameObjects[g_CurrentObjectIndex]->accel.z = 0.0f;
		break;
	case GLFW_KEY_W:		
		if (action == GLFW_PRESS)
			::g_vecGameObjects[g_CurrentObjectIndex]->accel.z = -5.0f;
		else if (action == GLFW_RELEASE)
			::g_vecGameObjects[g_CurrentObjectIndex]->accel.z = 0.0f;
		break;
	
	
	case GLFW_KEY_LEFT:
		
		/*::g_vecGameObjects[g_CurrentObjectIndex]->rotation =
			::g_vecGameObjects[g_CurrentObjectIndex]->rotation * glm::quat(glm::vec3(0.0f, 0.1f, 0.0f));*/
		if (action == GLFW_PRESS)
		{
			if (g_UsedBullet > 21)
			{
				g_UsedBullet = 2;
			}
			g_vecGameObjects[g_UsedBullet]->position = g_vecGameObjects[1]->position;

			g_vecGameObjects[g_UsedBullet]->vel.x = -40.0f;
			g_vecGameObjects[g_UsedBullet]->vel.z = 0.0f;
			g_vecGameObjects[g_UsedBullet]->vel.y = 0.0f;
			g_UsedBullet++;
			g_LuaBrain->RunScriptImmediately("PlayAudio(1)\n print(\"playing laser sound!\")");
		}


		break;
	case GLFW_KEY_RIGHT:
		if (action == GLFW_PRESS)
		{
			if (g_UsedBullet > 21)
			{
				g_UsedBullet = 2;
			}
			g_vecGameObjects[g_UsedBullet]->position = g_vecGameObjects[1]->position;

			g_vecGameObjects[g_UsedBullet]->vel.x = 40.0f;
			g_vecGameObjects[g_UsedBullet]->vel.z = 0.0f;
			g_vecGameObjects[g_UsedBullet]->vel.y = 0.0f;
			g_UsedBullet++;
			g_LuaBrain->RunScriptImmediately("PlayAudio(1)\n print(\"playing laser sound!\")");
		}
		break;
	case GLFW_KEY_UP:
		if (action == GLFW_PRESS)
		{
			if (g_UsedBullet > 21)
			{
				g_UsedBullet = 2;
			}
			g_vecGameObjects[g_UsedBullet]->position = g_vecGameObjects[1]->position;
			
			g_vecGameObjects[g_UsedBullet]->vel.z = -40.0f;
			g_vecGameObjects[g_UsedBullet]->vel.x = 0.0f;
			g_vecGameObjects[g_UsedBullet]->vel.y = 0.0f;
			g_UsedBullet++;
			g_LuaBrain->RunScriptImmediately("PlayAudio(1)\n print(\"playing laser sound!\")");
		}
		break;
	case GLFW_KEY_DOWN:
		if (action == GLFW_PRESS)
		{
			if (g_UsedBullet > 21)
			{
				g_UsedBullet = 2;
			}
			g_vecGameObjects[g_UsedBullet]->position = g_vecGameObjects[1]->position;

			g_vecGameObjects[g_UsedBullet]->vel.z = 40.0f;
			g_vecGameObjects[g_UsedBullet]->vel.x = 0.0f;
			g_vecGameObjects[g_UsedBullet]->vel.y = 0.0f;
			g_UsedBullet++;
			g_LuaBrain->RunScriptImmediately("PlayAudio(1)\n print(\"playing laser sound!\")");
		}

		break;
	case GLFW_KEY_1:
		if (action == GLFW_PRESS)
		if (g_DbID > 0)
		{
			g_DbID--;
		}
		break;
	case (GLFW_KEY_2):
		if(action == GLFW_PRESS)
		g_DbID++;
		break;
	case (GLFW_KEY_3):
		if (action == GLFW_PRESS)
		{
			GetHighScoreId(g_DbID);
		}
		break;
	case (GLFW_KEY_4):
		if (action == GLFW_PRESS)
		{
			SetHighScore(g_DbID, g_Score);
		}
		break;

	case (GLFW_KEY_TAB):
		if (action == GLFW_RELEASE)
		{
			if (g_CurrentLightIndex < g_LightColourStorage.size() - 1)
			{
				g_CurrentLightIndex++;
			}
			else
			{
				g_CurrentLightIndex = 0;
			}
		}
		break;
	case (GLFW_KEY_ENTER):
		if (action == GLFW_RELEASE)
		{
			if (g_CurrentObjectIndex < g_vecGameObjects.size() - 1)
			{
				g_CurrentObjectIndex++;
			}
			else
			{
				g_CurrentObjectIndex = 0;
			}
		}
		break;
	case (GLFW_KEY_G):
		if (action == GLFW_RELEASE)
		{
			::g_vecGameObjects[g_CurrentObjectIndex]->isWireFrame = !(::g_vecGameObjects[g_CurrentObjectIndex]->isWireFrame);
		}
	
	}

	return;
}
/*
* Name: LoadMeshIntoVAO
* Purpose: Loads a Model and then proceds to load it into the VAO
* Accepts: a string for a file name, a string for the mesh name, a GLint for the shader ID
* Returns: Nothing
*/
void LoadMeshIntoVAO(std::string fileName, std::string meshName, GLint sID)
{
	Mesh testMesh;
	testMesh.name = meshName;
	if (!LoadPlyFileIntoMeshWithNormals(fileName, testMesh))
	{
		std::cout << "Didn't load model" << std::endl;
	
	}
	if (!::g_pVAOManager.loadMeshIntoVAO(testMesh, sID))
	{
		std::cout << "Could not load mesh into VAO" << std::endl;
	}
	testMesh.GeneratePhysicsTriangles();
	g_Meshes.insert(std::pair<std::string, Mesh>(meshName, testMesh));
}



/*
* Name: LoadInfoFromXMLFile
* Purpose: Load contents from an XML file into local variables
* Accepts: window height, width, and title for the window, vertex shader and fragment shader name (all by reference)
* Returns: Nothing
*/
void LoadInfoFromXMLFile(int &w, int &h, std::string &t, std::string &vs, std::string &fs)
{
	std::ifstream infoFile("assets//files//config.xml");

	if (!infoFile.is_open())
	{	// File didn't open...
		std::cout << "Can't find config file" << std::endl;
		std::cout << "Using defaults" << std::endl;
	}
	else
	{	// File DID open, so read it... 
		ReadFileToToken(infoFile, "<Width>");
		infoFile >> w;
		ReadFileToToken(infoFile, "<Height>");
		infoFile >> h;
		ReadFileToToken(infoFile, "<Title>");
		infoFile >> t;
		ReadFileToToken(infoFile, "<Vertex>");
		infoFile >> vs;
		ReadFileToToken(infoFile, "<Fragment>");
		infoFile >> fs;
	}
};
/*
* Name: LoadLightInfoFromFile
* Purpose: Load contents from a file into local light variable
* Accepts: Nothing
* Returns: Nothing
*/
void LoadLightInfoFromFile()
{
	std::ifstream infoFile("assets//files//lights.txt");

	if (!infoFile.is_open())
	{	// File didn't open...
		std::cout << "Can't find light file" << std::endl;
	}
	else
	{	// File DID open, so read it... 

		for (Light &light : g_pLightManager->vecLights)
		{
			ReadFileToToken(infoFile, "atten:");
			infoFile >> light.attenuation.y;
			ReadFileToToken(infoFile, "x:");
			infoFile >> light.position.x;
			ReadFileToToken(infoFile, "y:");
			infoFile >> light.position.y;
			ReadFileToToken(infoFile, "z:");
			infoFile >> light.position.z;
			ReadFileToToken(infoFile, "r:");
			infoFile >> light.diffuse.r;
			ReadFileToToken(infoFile, "g:");
			infoFile >> light.diffuse.g;
			ReadFileToToken(infoFile, "b:");
			infoFile >> light.diffuse.b;
		}
	}
}
/*
* Name: LoadModelLocations
* Purpose: Load contents from a file into local model variable
* Accepts: Nothing
* Returns: Nothing
*/
void LoadModelLocations(GLint sexyShaderID)
{
	std::ifstream infoFile("assets//files//models.txt");
	std::string tempLocation;
	std::string tempName;
	if (!infoFile.is_open())
	{	// File didn't open...
		std::cout << "Can't find model location file" << std::endl;
	}
	else
	{	// File DID open, so read it... 
		while (infoFile.peek() != std::ifstream::traits_type::eof())
		{
			ReadFileToToken(infoFile, "location:");
			infoFile >> tempLocation;
			ReadFileToToken(infoFile, "name:");
			infoFile >> tempName;

			LoadMeshIntoVAO(tempLocation, tempName, sexyShaderID);
		}
	}
}
/*
* Name: CreateGameObject
* Purpose: Initialize and add a new GameObject to the global vector
* Accepts: position(xyz), scale, axis rotation, origin rotation, diffuse colour,
*		   meshName, collision type, physics flag
* Returns: Nothing
*/
void CreateGameObject(glm::vec3 pos, float scale, glm::vec4 diffuse
	, std::string meshName, eTypeOfObject typeOfObject, bool isUpdatedPhysics, float radius, glm::vec3 velocity)
{

	//TODO
	//Could call XML reader here...

	GameObject* tempObject = new GameObject();
	tempObject->position = pos;
	//tempObject->orientation = glm::radians(rotationA);	// Degrees
	//tempObject->orientation2 = glm::radians(rotationO);	// Degrees   
	tempObject->scale = scale;
	tempObject->diffuseColour = diffuse;
	tempObject->meshName = meshName;
	tempObject->typeOfObject = typeOfObject;
	tempObject->vel.x = velocity.x;
	tempObject->IsUpdatedInPhysics = isUpdatedPhysics;
	tempObject->radius = radius;
	::g_vecGameObjects.push_back(tempObject);

};
/*
* Name: LoadGameObjectsFromJSON
* Purpose: Load contents from a JSON file into games objects vector
* Accepts: Nothing
* Returns: Nothing
*/
void LoadGameObjectsFromJSON()
{
	FILE* fp = fopen("assets//files//gameobjects.json", "rb");
	char readBuffer[65536];
	rapidjson::FileReadStream is(fp, readBuffer, sizeof(readBuffer));
	rapidjson::Document d;
	d.ParseStream(is);
	
	bool keepReading = true;
	int objectNum = 1;
	std::stringstream ss;

	glm::vec3 tempPos;
	float tempScale;
	/*glm::vec3 tempRot1;
	glm::vec3 tempRot2;*/
	glm::vec4 tempColour;
	std::string tempName;
	std::string tempEnumType;
	eTypeOfObject tempType;
	float tempRadius;
	bool tempPhysics;

	while (keepReading)
	{
	
		ss << "Object";
		ss << objectNum;
		std::string nameToFind;
		ss >> nameToFind;
		ss >> nameToFind;
		ss.clear();
		if (!d.HasMember(nameToFind.c_str()))
		{
			keepReading = false;
			continue;
		}
		rapidjson::Value& obj = d[nameToFind.c_str()];
		tempPos.x = obj["x"].GetDouble();
		tempPos.y = obj["y"].GetDouble();
		tempPos.z = obj["z"].GetDouble();
		tempScale = obj["scale"].GetDouble();
		/*tempRot1.x = obj["rot1x"].GetDouble();
		tempRot1.y = obj["rot1y"].GetDouble();
		tempRot1.z = obj["rot1z"].GetDouble();
		tempRot2.x = obj["rot2x"].GetDouble();
		tempRot2.y = obj["rot2y"].GetDouble();
		tempRot2.z = obj["rot2z"].GetDouble();*/
		tempColour.r = obj["r"].GetDouble();
		tempColour.g = obj["g"].GetDouble();
		tempColour.b = obj["b"].GetDouble();
		tempName = obj["model"].GetString();
		tempEnumType = obj["type"].GetString();
		tempPhysics = obj["physics"].GetBool();
		tempRadius = obj["radius"].GetDouble();
		tempType = static_cast<eTypeOfObject>(atoi(tempEnumType.c_str()));

		CreateGameObject(tempPos, tempScale,
			tempColour, tempName, tempType, tempPhysics, tempRadius, glm::vec3(0.0f, 0.0f, 0.0f));
		
		objectNum++;
	}
	
	fclose(fp);	
}
/*
* Name: RandomNumber
* Purpose: generate a random value between two inputed values
* Accepts: 2 floats
* Returns: a randomly generated number between the 2 floats
*/
float RandomNumber(float Min, float Max)
{
	
	return ((float(rand()) / float(RAND_MAX)) * (Max - Min)) + Min;
}

/*
* Name: ResetVirus
* Purpose: generate a random location and velocity for a virus
* Accepts: a pointer to a game object
* Returns: Nothing
*/
void ResetVirus(GameObject* pVirus)
{
	
	float randX = round(RandomNumber(-100, 150) / 10.0f) * 10.0f;
	float randZ = round(RandomNumber(-90, -100) / 10.0f) * 10.0f;

	float velX =  RandomNumber(-5, 10);
	float velZ = RandomNumber(5, 15);

	pVirus->position.y = 30.0f;
	pVirus->position.x = randX;
	pVirus->position.z = randZ;

	pVirus->vel.x = velX;
	pVirus->vel.z = velZ;

	pVirus->IsUpdatedInPhysics = true;
}

/*
* Name: GenerateViruses
* Purpose: create a number of viruses and reset their position
* Accepts: an int to determine number of viruses to generate
* Returns: Nothing
*/
void GenerateViruses(int numToGenerate)
{
	for (int i = 0; i < numToGenerate; i++)
	{
		g_VirusFactory->CreateVirus("basic");
		ResetVirus(g_vecGameObjects[g_vecGameObjects.size()-1]);
	}
}

/*
* Name: GenerateBullets
* Purpose: create a number of bullets sets their position off the scene
* Accepts: Nothing
* Returns: Nothing
*/
void GenerateBullets()
{
	for (int i = 0; i < 20; i++)
	{
		CreateGameObject(glm::vec3(-50.0f),1.0f,glm::vec4(0.0f,1.0f,1.0f,1.0f),"bullet",eTypeOfObject::SPHERE,true,1.0f,glm::vec3(0.0f));
	}
}

int main(void)
{

	GLFWwindow* window;
	//    GLuint vertex_buffer, vertex_shader, fragment_shader, program;
	GLint mvp_location;	// , vpos_location, vcol_location;
	glfwSetErrorCallback(error_callback);

	srand(time(NULL));


	if (!glfwInit())
		exit(EXIT_FAILURE);

	int height = 480;	/* default */
	int width = 640;	// default
	std::string title = "OpenGL Rocks";
	std::string vertShaderName = "";
	std::string fragShaderName = "";
	LoadInfoFromXMLFile(width, height, title, vertShaderName, fragShaderName);

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);

	window = glfwCreateWindow(width, height,
		title.c_str(),
		NULL, NULL);
	if (!window)
	{
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	glfwSetKeyCallback(window, key_callback);
	glfwMakeContextCurrent(window);
	gladLoadGLLoader((GLADloadproc)glfwGetProcAddress);
	glfwSwapInterval(1);

	std::cout << glGetString(GL_VENDOR) << " "
		<< glGetString(GL_RENDERER) << ", "
		<< glGetString(GL_VERSION) << std::endl;
	std::cout << "Shader language version: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << std::endl;

	::g_pShaderManager = new ShaderManager();

	//Populate game objects vector
	LoadGameObjectsFromJSON();

	ShaderManager::Shader vertShader;
	ShaderManager::Shader fragShader;

	vertShader.fileName = vertShaderName;
	fragShader.fileName = fragShaderName;

	::g_pShaderManager->setBasePath("assets//shaders//");

	// Shader objects are passed by reference so that
	//	we can look at the results if we wanted to. 
	if (!::g_pShaderManager->createProgramFromFile(
		"mySexyShader", vertShader, fragShader))
	{
		std::cout << "Oh no! All is lost!!! Blame Loki!!!" << std::endl;
		std::cout << ::g_pShaderManager->getLastError() << std::endl;
		// Should we exit?? 
		return -1;
		//		exit(
	}
	std::cout << "The shaders comipled and linked OK" << std::endl;


	// Load models
	::g_pVAOManager = VAOMeshManager::getInstance();
	GLint sexyShaderID = ::g_pShaderManager->getIDFromFriendlyName("mySexyShader");
	LoadModelLocations(sexyShaderID);
	GLint currentProgID = ::g_pShaderManager->getIDFromFriendlyName("mySexyShader");

	// Get the uniform locations for this shader
	mvp_location = glGetUniformLocation(currentProgID, "MVP");		// program, "MVP");
	uniLoc_materialDiffuse = glGetUniformLocation(currentProgID, "materialDiffuse");
	uniLoc_materialAmbient = glGetUniformLocation(currentProgID, "materialAmbient");
	uniLoc_ambientToDiffuseRatio = glGetUniformLocation(currentProgID, "ambientToDiffuseRatio");
	uniLoc_materialSpecular = glGetUniformLocation(currentProgID, "materialSpecular");
	uniLoc_eyePosition = glGetUniformLocation(currentProgID, "eyePosition");

	uniLoc_mModel = glGetUniformLocation(currentProgID, "mModel");
	uniLoc_mView = glGetUniformLocation(currentProgID, "mView");
	uniLoc_mProjection = glGetUniformLocation(currentProgID, "mProjection");

	//	GLint uniLoc_diffuseColour = glGetUniformLocation( currentProgID, "diffuseColour" );

	::g_pLightManager = new LightManager();

	::g_pLightManager->CreateLights(1);	// There are 10 lights in the shader
	::g_pLightManager->LoadShaderUniformLocations(currentProgID);
	LoadLightInfoFromFile();
	//Initialize Light storage
	for (int index = 0; index < g_pLightManager->vecLights.size(); index++)
	{
		g_LightColourStorage.push_back(glm::vec3(0.0f));
	}

	//Create audio manager
	::g_pAudioManager = new AudioManager();

	::g_pAudioManager->AddSound("assets//audio//background.mp3", FMOD_LOOP_NORMAL);
	::g_pAudioManager->AddSound("assets//audio//laser.wav", FMOD_DEFAULT);
	::g_pAudioManager->AddSound("assets//audio//blast.wav", FMOD_DEFAULT);
	::g_pAudioManager->PlaySound(0);

	//Lua Brain
	g_LuaBrain = new cLuaBrain();

	

	glEnable(GL_DEPTH);

	//Generate Bullets for the ship
	GenerateBullets();

	//Create instance of Virus Factory and add a virus
	g_VirusFactory = new VirusFactory();
	//Add 5 Viruses to the scene
	GenerateViruses(5);
	
	
	//Open SQLite database
	if (!OpenSQLiteDB("highscores.db"))
	{
		return 1;
	}


	// Gets the "current" time "tick" or "step"
	double lastTimeStep = glfwGetTime();

	// Main game or application loop
	while (!glfwWindowShouldClose(window))
	{
		float ratio;
		int width, height;
		glm::mat4x4 m, p, mvp;			//  mat4x4 m, p, mvp;

		glfwGetFramebufferSize(window, &width, &height);
		ratio = width / (float)height;
		glViewport(0, 0, width, height);

		// Clear colour AND depth buffer
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		
		::g_pShaderManager->useShaderProgram("mySexyShader");
		GLint shaderID = ::g_pShaderManager->getIDFromFriendlyName("mySexyShader");

		// Update all the light uniforms...
		// (for the whole scene)
		::g_pLightManager->CopyLightInformationToCurrentShader();


		p = glm::perspective(0.6f,			// FOV
			ratio,		// Aspect ratio
			0.1f,			// Near (as big as possible)
			1000.0f);	// Far (as small as possible)

						// View or "camera" matrix
		glm::mat4 v = glm::mat4(1.0f);	// identity

										//glm::vec3 cameraXYZ = glm::vec3( 0.0f, 0.0f, 5.0f );	// 5 units "down" z
		v = glm::lookAt(g_CameraPos,						// "eye" or "camera" position
			g_CameraTarget,		// "At" or "target" 
			glm::vec3(0.0f, 1.0f, 0.0f));	// "up" vector

		glUniformMatrix4fv(uniLoc_mView, 1, GL_FALSE,
			(const GLfloat*)glm::value_ptr(v));
		glUniformMatrix4fv(uniLoc_mProjection, 1, GL_FALSE,
			(const GLfloat*)glm::value_ptr(p));


		unsigned int sizeOfVector = ::g_vecGameObjects.size();	//*****//
		for (int index = 0; index != sizeOfVector; index++)
		{

			GameObject* pTheGO = ::g_vecGameObjects[index];

			DrawObject(pTheGO);

		}//for ( int index = 0...

		std::stringstream ssTitle;
		ssTitle << title <<
			"          Database ID, Database Score: "
			<< g_DbID << ", "
			<< " " << g_Score
			<< "        Current Object: "
			<< g_CurrentObjectIndex
			<< "         Camera (xyz): "
			<< g_CameraPos.x << ", "
			<< g_CameraPos.y << ", "
			<< g_CameraPos.z;
		glfwSetWindowTitle(window, ssTitle.str().c_str());


		double curTime = glfwGetTime();
		double deltaTime = curTime - lastTimeStep;

		PhysicsStep(deltaTime);

		lastTimeStep = curTime;

		glfwSwapBuffers(window);
		glfwPollEvents();



	}// while ( ! glfwWindowShouldClose(window) )


	glfwDestroyWindow(window);
	glfwTerminate();

	// 
	delete ::g_pShaderManager;
	//delete ::g_pVAOManager;
	CloseSQLiteDB();
	//    exit(EXIT_SUCCESS);
	return 0;
}

/*
* Name: DrawObject
* Purpose: Draw an Object to the screen
* Accepts: a pointer to a GameObject
* Returns: Nothing
*/
void DrawObject(GameObject* pGO)
{
	// Is there a game object? 
	if (pGO == 0)	//if ( ::g_GameObjects[index] == 0 )
	{	// Nothing to draw
		return;		// Skip all for loop code and go to next
	}

	std::string meshToDraw = pGO->meshName;		//::g_GameObjects[index]->meshName;

	sVAOInfo VAODrawInfo;
	if (::g_pVAOManager.lookupVAOFromName(meshToDraw, VAODrawInfo) == false)
	{	// Didn't find mesh
		return;
	}

	glm::mat4x4 mModel = glm::mat4(1.0f);

	glm::mat4x4 trans = glm::mat4x4(1.0f);
	trans = glm::translate(trans,
		pGO->position);
	mModel = mModel * trans;


	glm::mat4x4 RotationMatrix = glm::mat4x4(pGO->rotation);
	mModel = mModel * RotationMatrix;

	float finalScale = pGO->scale;

	glm::mat4x4 matScale = glm::mat4x4(1.0f);
	matScale = glm::scale(matScale,
		glm::vec3(finalScale,
			finalScale,
			finalScale));
	mModel = mModel * matScale;

	::g_pShaderManager->useShaderProgram("mySexyShader");
	GLint shaderID = ::g_pShaderManager->getIDFromFriendlyName("mySexyShader");

	glUniformMatrix4fv(uniLoc_mModel, 1, GL_FALSE,
		(const GLfloat*)glm::value_ptr(mModel));
	
	glm::mat4 mWorldInTranpose = glm::inverse(glm::transpose(mModel));

	glUniform4f(uniLoc_materialDiffuse,
		pGO->diffuseColour.r,
		pGO->diffuseColour.g,
		pGO->diffuseColour.b,
		pGO->diffuseColour.a);
	//...and all the other object material colours



	//			glPolygonMode( GL_FRONT_AND_BACK, GL_POINT );
	if (pGO->isWireFrame)
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	}
	else
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);	// Default
	}
	glEnable(GL_DEPTH_TEST);		// Test for z and store in z buffer
	glCullFace(GL_BACK);


	glBindVertexArray(VAODrawInfo.VAO_ID);

	glDrawElements(GL_TRIANGLES,
		VAODrawInfo.numberOfIndices,		// testMesh.numberOfTriangles * 3,	// How many vertex indices
		GL_UNSIGNED_INT,					// 32 bit int 
		0);

	glBindVertexArray(0);
}




/*
* Name: PhysicsStep
* Purpose: Update the physics of all the GameObjects
* Accepts: a double of how much time has passed since the last update
* Returns: Nothing
*/
void PhysicsStep(double deltaTime)
{
	const glm::vec3 GRAVITY = glm::vec3(0.0f, 0.0f, 0.0f);



	// Identical to the 'render' (drawing) loop
	for (int index = 0; index != ::g_vecGameObjects.size(); index++)
	{
		GameObject* pCurGO = ::g_vecGameObjects[index];


		// Is this object to be updated?
		if (!pCurGO->IsUpdatedInPhysics)
		{	// DON'T update this
			continue;		// Skip everything else in the for
		}

		
		// Explicit Euler  (RK4) 
		// New position is based on velocity over time
		glm::vec3 deltaPosition = (float)deltaTime * pCurGO->vel;
		pCurGO->position += deltaPosition;

		// New velocity is based on acceleration over time
		glm::vec3 deltaVelocity = ((float)deltaTime * pCurGO->accel)
			+ ((float)deltaTime * GRAVITY);

		pCurGO->vel += deltaVelocity;


		//HACK Phase: (If any object reaches the limits, their position is set to the opposite side)
		//Do not do this for the bullets
		if (pCurGO->meshName != "bullet")
		{
			if (pCurGO->position.x > 150.0f || pCurGO->position.x < -150.0f)
			{
				pCurGO->position.x *= -1.0f;
			}
			if (pCurGO->position.z > 100.0f || pCurGO->position.z < -100.0f)
			{
				pCurGO->position.z *= -1.0f;
			}
		}

		switch (pCurGO->typeOfObject)
		{
		case eTypeOfObject::SPHERE:
			// Comare this to EVERY OTHER object in the scene
			for (int indexEO = 0; indexEO != ::g_vecGameObjects.size(); indexEO++)
			{
				// Don't test for myself
				if (index == indexEO)
					continue;	// It's me!! 

				GameObject* pOtherObject = ::g_vecGameObjects[indexEO];
				// Is Another object
				switch (pOtherObject->typeOfObject)
				{
				case eTypeOfObject::SPHERE:
					// 
					if (PenetrationTestSphereSphere(pCurGO, pOtherObject))
					{
						//if both are viruses
						if(pCurGO->meshName == "sphere" && pOtherObject->meshName == "sphere")
						PerformSphereSphereCollision(pCurGO, pOtherObject);
						else
						{
							//if one is a bullet
							if (pCurGO->meshName == "sphere")
							{
								//Reset the virus position
								ResetVirus(pCurGO);
								//Reset the bullet position
								pOtherObject->position = glm::vec3(-50.0f);
								g_LuaBrain->RunScriptImmediately("PlayAudio(2)\n print(\"playing virus destruction sound!\")");
								g_Score += 5;
							}
						}
					}
					break;
				case eTypeOfObject::UNKNOWN:
					if (PenetrationTestSphereSphere(pCurGO, pOtherObject))
					{
						//if the detection is with a virus
						if (pCurGO->meshName == "sphere")
						{
							//Ship with virus collision
							pOtherObject->diffuseColour = glm::vec4(1.0f, 0.0f, 0.0f, 1.0f);//set to red to indicate hit
						}
						
					}
					
					break;
				
				}//switch ( pOtherObject->typeOfObject )

			}


			break;
		};

	}//for ( int index...
	return;
}
