#ifndef _FastVirus_HG_
#define _FastVirus_HG_

#include "iVirus.h"
#include"VirusBomb.h"
class GameObject;	// forward declare
					//inherit interface
// Header Definition of FastVirus
// Name: FastVirus
// Purpose: The representation of what a "Fast Virus" will be during gameplay in terms of behaviour and properties
//			Contains inherited methods from the iVirus interface that influences what it can do
class FastVirus : public iVirus
{
public:
	FastVirus();
	virtual ~FastVirus();

	//Inherited methods
	virtual void FindTarget(void);
	virtual void SpawnChild(void);
	virtual void SpawnObjectOnDeath(void);

	virtual void SetPosition(glm::vec3 newPos);
	virtual void SetVelocity(glm::vec3 newVel);

	//TODO
	
	VirusBomb* pBomb;
	GameObject* pMesh;

private:	//Pimpl
	class ImplementationData;
	ImplementationData* pimpl;
};

#endif 
