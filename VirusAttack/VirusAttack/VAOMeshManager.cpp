#include <glad/glad.h>		
#include <GLFW/glfw3.h>		
#include "VAOMeshManager.h"
#include "Mesh.h"	

// struct for Vertex
// Name: Vertex
// Purpose: Will be used to store information for a vertex.
struct Vertex
{
public:
	float x, y, z;			
	float r, g, b;		
	float nx, ny, nz;	
};



//Destructor
VAOMeshManager::~VAOMeshManager()
{

	return;
}

 VAOMeshManager& VAOMeshManager::getInstance()
{
	 static VAOMeshManager instance;

	 return instance;
}

/*
* Name: loadMeshIntoVAO
* Purpose: Take name from mesh and store it for lookup for later rendering
* Accepts: a Mesh object which information is being taken, and the Shader ID of the shader that's being used
* Returns: true if loading the Mesh succeeds or false if it fails
*/
bool VAOMeshManager::loadMeshIntoVAO(Mesh &theMesh, int shaderID)
{
	sVAOInfo theVAOInfo;

	// Create a Vertex Array Object (VAO)
	glGenVertexArrays(1, &(theVAOInfo.VAO_ID));
	glBindVertexArray(theVAOInfo.VAO_ID);


	glGenBuffers(1, &(theVAOInfo.vertex_buffer_ID));
	glBindBuffer(GL_ARRAY_BUFFER, theVAOInfo.vertex_buffer_ID);

	// Allocate the global vertex array
	Vertex* pVertices = new Vertex[theMesh.numberOfVertices];

	for (int index = 0; index < theMesh.numberOfVertices; index++)
	{
		pVertices[index].x = theMesh.pVertices[index].pos.x;
		pVertices[index].y = theMesh.pVertices[index].pos.y;
		pVertices[index].z = theMesh.pVertices[index].pos.z;

		pVertices[index].r = theMesh.pVertices[index].col.r;
		pVertices[index].g = theMesh.pVertices[index].col.g;
		pVertices[index].b = theMesh.pVertices[index].col.b;

		pVertices[index].nx = theMesh.pVertices[index].npos.x;
		pVertices[index].ny = theMesh.pVertices[index].npos.y;
		pVertices[index].nz = theMesh.pVertices[index].npos.z;
	}

	// Copy the local vertex array into the GPUs memory
	int sizeOfGlobalVertexArrayInBytes = sizeof(Vertex) * theMesh.numberOfVertices;
	glBufferData(GL_ARRAY_BUFFER,
		sizeOfGlobalVertexArrayInBytes,		// sizeof(vertices), 
		pVertices,
		GL_STATIC_DRAW);

	// Get rid of local vertex array
	delete[] pVertices;
	

	glGenBuffers(1, &(theVAOInfo.index_buffer_ID));
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, theVAOInfo.index_buffer_ID); // It's now an Index Buffer

	int numberOfIndices = theMesh.numberOfTriangles * 3;

	
	unsigned int* indexArray = new unsigned int[numberOfIndices];

	// Copy the triangle data into this 1D array 
	int triIndex = 0;		// Index into the triangle array (from mesh)
	int indexIndex = 0;		// Index into the "vertex index" array (1D)
	for (; triIndex < theMesh.numberOfTriangles; triIndex++, indexIndex += 3)
	{
		indexArray[indexIndex + 0] = theMesh.pTriangles[triIndex].x;
		indexArray[indexIndex + 1] = theMesh.pTriangles[triIndex].y;
		indexArray[indexIndex + 2] = theMesh.pTriangles[triIndex].z;
	}

	int sizeOfIndexArrayInBytes = sizeof(unsigned int) * numberOfIndices;

	glBufferData(GL_ELEMENT_ARRAY_BUFFER,		// "index buffer" or "vertex index buffer"
		sizeOfIndexArrayInBytes,
		indexArray,
		GL_STATIC_DRAW);

	
	delete[] indexArray;



	GLuint vpos_location = glGetAttribLocation(shaderID, "vPos");		
	GLuint vcol_location = glGetAttribLocation(shaderID, "vCol");		
	GLuint vnorm_location = glGetAttribLocation(shaderID, "vNorm");		

																		
																		
	glEnableVertexAttribArray(vpos_location);
	glVertexAttribPointer(vpos_location,
		3,				
		GL_FLOAT,
		GL_FALSE,
		sizeof(float) * 9,
									
		(void*)offsetof(Vertex, x));


	glEnableVertexAttribArray(vcol_location);
	glVertexAttribPointer(vcol_location,
		3,
		GL_FLOAT,
		GL_FALSE,
		sizeof(float) * 9,		
		(void*)offsetof(Vertex, r));

	
	glEnableVertexAttribArray(vnorm_location);
	glVertexAttribPointer(vnorm_location,
		3,
		GL_FLOAT,
		GL_FALSE,
		sizeof(float) * 9,		
		(void*)offsetof(Vertex, nx));

	// Copy the information into the VAOInfo structure
	theVAOInfo.numberOfIndices = theMesh.numberOfTriangles * 3;
	theVAOInfo.numberOfTriangles = theMesh.numberOfTriangles;
	theVAOInfo.numberOfVertices = theMesh.numberOfVertices;
	theVAOInfo.friendlyName = theMesh.name;
	theVAOInfo.shaderID = shaderID;


	theMesh.CalculateExtents();
	theVAOInfo.scaleForUnitBBox = theMesh.scaleForUnitBBox;

	// Store the VAO info by mesh name
	this->m_mapNameToVAO[theMesh.name] = theVAOInfo;

	glBindVertexArray(0);	 

	// Unbind (release) everything
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glDisableVertexAttribArray(vcol_location);
	glDisableVertexAttribArray(vpos_location);

	return true;
}

/*
* Name: lookupVAOFromName
* Purpose: Retrieve the rendering information for mesh by looking it by Mesh name
* Accepts: the name of the mesh whose information we're trying to retrieve,
*          an sVAOInfo by reference that will be pushed with the information found during the mesh lookup
* Returns: true if loading the lookup succeeds or false if it fails
*/
bool VAOMeshManager::lookupVAOFromName(std::string name, sVAOInfo &theVAOInfo)
{
	// look up in map for the name of the mesh we want to draw
	std::map< std::string, sVAOInfo >::iterator itVAO = this->m_mapNameToVAO.find(name);

	//Was something found?
	if (itVAO == this->m_mapNameToVAO.end())
	{	
		return false;
	}

	theVAOInfo = itVAO->second;		
	return true;
}
