#ifndef _VirusFactory_HG_
#define _VirusFactory_HG_

#include"iVirus.h" //interface
#include<string>
// Header Definition of VirusFactory
// Name: VirusFactory
// Purpose: An object that will accept a string in it's methods and generate a new virus object based on what was passed in
class VirusFactory
{
public:
	/*
	* Name: CreateVirus
	* Purpose: create a virus object and add it to the vector of viruses (Factory)
	* Accepts: a string to determine which virus to make
	* Returns: Nothing
	*/
	iVirus* CreateVirus(std::string virusType);

	/*
	* Name: AssembleVirus
	* Purpose: create and add various componenets to a virus (Builder)
	* Accepts: a pointer to an iVirus, and a string to determine virus type
	* Returns: Nothing
	*/
	void AssembleVirus(iVirus* pVirus, std::string virusType);

};

#endif 
