#ifndef _ModelUtilities_HG_
#define _ModelUtilities_HG_

#include <fstream>
#include <string>
#include "Mesh.h"

/*
* Name: ReadFileToToken
* Purpose: Reads file until the passed in token is found
* Accepts: an inputstream by reference, and the token string to stop reading the file at
* Returns: Nothing
*/
void ReadFileToToken(std::ifstream &file, std::string token);

/*
* Name: LoadPlyFileIntoMesh
* Purpose: reads information from a ply file and pushes that information into the passed in Mesh objet
* Accepts: the filename to read from, and the Mesh object to add information to
* Returns: true if succeced and false if it failed
*/
bool LoadPlyFileIntoMesh(std::string filename, Mesh &theMesh);
/*
* Name: LoadPlyFileIntoMeshWithNormals
* Purpose: reads information from a ply file including normals and pushes that information into the passed in Mesh objet
* Accepts: the filename to read from, and the Mesh object to add information to
* Returns: true if succeced and false if it failed
*/
bool LoadPlyFileIntoMeshWithNormals(std::string filename, Mesh &theMesh);



#endif
