#ifndef _GiantVirus_HG_
#define _GiantVirus_HG_

#include "iVirus.h"
class GameObject;	// forward declare
					//inherit interface
// Header Definition of GiantVirus
// Name: GiantVirus
// Purpose: The representation of what a "Giant Virus" will be during gameplay in terms of behaviour and properties
//			Contains inherited methods from the iVirus interface that influences what it can do
class GiantVirus : public iVirus
{
public:
	GiantVirus();
	virtual ~GiantVirus();

	//Inherited methods
	virtual void FindTarget(void);
	virtual void SpawnChild(void);
	virtual void SpawnObjectOnDeath(void);

	virtual void SetPosition(glm::vec3 newPos);
	virtual void SetVelocity(glm::vec3 newVel);

	//TODO
	//PowerUp* pPowerUp;

	GameObject* pMesh;

private:	//Pimpl
	class ImplementationData;
	ImplementationData* pimpl;
};

#endif 
