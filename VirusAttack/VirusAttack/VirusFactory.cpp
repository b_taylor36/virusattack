#include"VirusFactory.h"
#include "GameObject.h"
#include"Virus.h"
#include"GiantVirus.h"
#include"FastVirus.h"
#include"GigaVirus.h"
#include <vector>
#include <glm/glm.hpp>

extern std::vector< GameObject* >  g_vecGameObjects; //from Main

iVirus* VirusFactory::CreateVirus(std::string virusType)
{
	//TODO
	iVirus* pVirus = NULL;

	if (virusType == "basic")
	{
		pVirus = new Virus();
		GameObject* pTempGO = new GameObject();
		pTempGO->scale = 1.0f;
		pTempGO->radius = 12.0f;
		pTempGO->diffuseColour = glm::vec4(0.0f, 1.0f, 0.0f, 1.0f);
		pTempGO->meshName = "sphere";
		pTempGO->typeOfObject = eTypeOfObject::SPHERE;
		pTempGO->IsUpdatedInPhysics = false;
		pTempGO->isWireFrame = false;
		::g_vecGameObjects.push_back(pTempGO);		
		Virus* pV = (Virus*)pVirus;		
		pV->pMesh = pTempGO;
		this->AssembleVirus(pV, virusType);
		return pV;
	}

	else if (virusType == "giant")
	{
		pVirus = new GiantVirus();
		GameObject* pTempGO = new GameObject();
		pTempGO->scale = 1.0f; //TODO CHANGE
		pTempGO->diffuseColour = glm::vec4(1.0f, 0.0f, 0.0f, 1.0f);
		pTempGO->meshName = "sphere";
		pTempGO->typeOfObject = eTypeOfObject::SPHERE;
		pTempGO->IsUpdatedInPhysics = false;
		pTempGO->isWireFrame = false;
		::g_vecGameObjects.push_back(pTempGO);
		GiantVirus* pV = (GiantVirus*)pVirus;
		pV->pMesh = pTempGO;
		this->AssembleVirus(pV, virusType);
		return pV;
	}
	else if (virusType == "fast")
	{
		pVirus = new FastVirus();
		GameObject* pTempGO = new GameObject();
		pTempGO->scale = 1.0f; 
		pTempGO->diffuseColour = glm::vec4(0.0f, 0.0f, 1.0f, 1.0f);
		pTempGO->meshName = "sphere";
		pTempGO->typeOfObject = eTypeOfObject::SPHERE;
		pTempGO->IsUpdatedInPhysics = false;
		pTempGO->isWireFrame = false;
		::g_vecGameObjects.push_back(pTempGO);
		FastVirus* pV = (FastVirus*)pVirus;
		pV->pMesh = pTempGO;
		this->AssembleVirus(pV, virusType);
		return pV;
	}
	else if (virusType == "giga")
	{
		pVirus = new GigaVirus();
		GameObject* pTempGO = new GameObject();
		pTempGO->scale = 1.0f;
		pTempGO->diffuseColour = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
		pTempGO->meshName = "sphere";
		pTempGO->typeOfObject = eTypeOfObject::SPHERE;
		pTempGO->IsUpdatedInPhysics = false;
		pTempGO->isWireFrame = false;
		::g_vecGameObjects.push_back(pTempGO);
		GigaVirus* pV = (GigaVirus*)pVirus;
		pV->pMesh = pTempGO;
		this->AssembleVirus(pV, virusType);
		return pV;
	}
	return pVirus;
}


void VirusFactory::AssembleVirus(iVirus* pTheVirus, std::string virusType)
{
	
	if (virusType == "basic")
	{	
		Virus* pVirus = (Virus*) pTheVirus ;	
		
	}
	else if (virusType == "fast")
	{	
		FastVirus* pVirus = (FastVirus*)pTheVirus;	
											
		pVirus->pBomb = new VirusBomb();
	}
	else if (virusType == "giant")
	{	
		GiantVirus* pVirus = (GiantVirus*)pTheVirus;	
		
		//pVirus->pPowerUp = new PowerUp();
	}
	else if (virusType == "giga")
	{
		GigaVirus* pVirus = (GigaVirus*)pTheVirus;

		//pVirus->powerUp = new PowerUp();
		pVirus->virusBomb = new VirusBomb();
	}
}