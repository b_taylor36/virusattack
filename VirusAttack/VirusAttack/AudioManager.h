#ifndef AudioManager_HG
#define AudioManager_HG


#include <fmod.hpp>
#include <fmod_errors.h>
#include<vector>

//Globals



class AudioManager
{
public:
	//Constructor/Destructor
	AudioManager();
	~AudioManager();

	//Methods
	void AddSound(char* filename, FMOD_MODE mode);

	void PlaySound(int index);

	void StopSound(int index);

};

#endif // !AudioManager_HG
