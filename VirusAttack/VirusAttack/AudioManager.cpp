#include"AudioManager.h"

FMOD_RESULT mresult;
FMOD::System *msystem;

std::vector<FMOD::Sound *>msounds;
std::vector<FMOD::Channel *>mchannels;

void errorcheck(FMOD_RESULT result)
{
	if (result != FMOD_OK)
	{
		fprintf(stderr, "FMOD error! (%d) %s\n", result, FMOD_ErrorString(result));
		exit(-1);
	}
}

AudioManager::AudioManager()
{
	// Create the main system object.
	mresult = FMOD::System_Create(&msystem);
	if (mresult != FMOD_OK)
	{
		fprintf(stderr, "FMOD error! (%d) %s\n", mresult, FMOD_ErrorString(mresult));
		exit(-1);
	}

	//Initializes the system object, and the msound device. This has to be called at the start of the user's program
	mresult = msystem->init(512, FMOD_INIT_NORMAL, 0);    // Initialize FMOD.
	if (mresult != FMOD_OK)
	{
		fprintf(stderr, "FMOD error! (%d) %s\n", mresult, FMOD_ErrorString(mresult));
		exit(-1);
	}
}

AudioManager::~AudioManager()
{
	//Shutdown
	for (int i = 0; i < msounds.size(); i++)
	{
		if (msounds[i])
		{
			msounds[i]->release();
			errorcheck(mresult);
		}
	}

	if (msystem)
	{
		mresult = msystem->close();
		errorcheck(mresult);
		mresult = msystem->release();
		errorcheck(mresult);
	}

}

void AudioManager::AddSound(char* filename, FMOD_MODE mode)
{
	FMOD::Sound* s;
	FMOD::Channel* c;
	mresult = msystem->createSound(filename, mode, 0, &s);
	errorcheck(mresult);

	msounds.push_back(s);
	mchannels.push_back(c);
}

void AudioManager::PlaySound(int index)
{
	mresult = msystem->playSound(msounds[index], 0, false, &mchannels[index]);
	errorcheck(mresult);

	mresult = mchannels[index]->setMute(false);
	errorcheck(mresult);
}

void AudioManager::StopSound(int index)
{
	mresult = msystem->playSound(msounds[index], 0, true, &mchannels[index]);
	errorcheck(mresult);

	mresult = mchannels[index]->setMute(true);
	errorcheck(mresult);
}