#ifndef _VertexInfo_HG_
#define _VertexInfo_HG_
#include<glm/vec3.hpp>
// Header Definition of VertexInfo
// Name: VertexInfo
// Purpose: Store Relevant information the individual vertices of the a Mesh
class VertexInfo
{
public:
	//Constructor/Destructor
	VertexInfo();
	~VertexInfo();
	glm::vec3 pos;
	glm::vec3 col;
	glm::vec3 npos;
};

#endif // !_VertexInfo_HG
