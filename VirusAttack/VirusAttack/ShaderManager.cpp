#include "ShaderManager.h"
#include <glad/glad.h>	
#include <fstream>
#include <sstream>		
#include <vector>
#include <algorithm>	
#include <iterator>		
#include <iostream>
//Constructor
ShaderManager::ShaderManager()
{
	return;
}
//Destructor
ShaderManager::~ShaderManager()
{
	return;
}

/*
* Name: useShaderProgram
* Purpose: uses Shader based on which shader ID is given
* Accepts: a Shader ID
* Returns: true if the the shader is being used
*/
bool ShaderManager::useShaderProgram(unsigned int ID)
{

	glUseProgram(ID);
	return true;
}

/*
* Name: useShaderProgram
* Purpose: uses Shader based on which shader name is given
* Accepts: a Shader friendly name
* Returns: true if the the shader is being used
*/
bool ShaderManager::useShaderProgram(std::string friendlyName)
{
	std::map< std::string /*name*/, unsigned int /*ID*/ >::iterator
		itShad = this->m_name_to_ID.find(friendlyName);

	if (itShad == this->m_name_to_ID.end())
	{	
		return false;
	}
	glUseProgram(itShad->second);

	return true;
}

/*
* Name: getIDFromFriendlyName
* Purpose: to obtain a shader ID from a shader friendly name
* Accepts: a string of the friendly shader name
* Returns: a Shader ID
*/
unsigned int ShaderManager::getIDFromFriendlyName(std::string friendlyName)
{
	std::map< std::string /*name*/, unsigned int /*ID*/ >::iterator
		itShad = this->m_name_to_ID.find(friendlyName);

	if (itShad == this->m_name_to_ID.end())
	{	// Didn't find it
		return 0;
	}
	return itShad->second;
}

const unsigned int MAXLINELENGTH = 65536;		// 16x1024


/*
* Name: setBasePath
* Purpose: changes the base path for looking for shaders
* Accepts: a string of the basepath
* Returns: Nothing
*/
void ShaderManager::setBasePath(std::string basepath)
{
	this->m_basepath = basepath;
	return;
}


/*
* Name: m_loadSourceFromFile
* Purpose: Load Shader file information
* Accepts: a Shader variable by reference
* Returns: true if the file was read, false if the file failed to read
*/
bool ShaderManager::m_loadSourceFromFile(Shader &shader)
{
	std::string fullFileName = this->m_basepath + shader.fileName;

	std::ifstream theFile(fullFileName.c_str());
	if (!theFile.is_open())
	{
		return false;
	}


	shader.vecSource.clear();

	char pLineTemp[MAXLINELENGTH] = { 0 };
	while (theFile.getline(pLineTemp, MAXLINELENGTH))
	{
		std::string tempString(pLineTemp);
		//if ( tempString != "" )
		//{	// Line isn't empty, so add
		shader.vecSource.push_back(tempString);
		//}
	}

	theFile.close();
	return true;		// Return the string (from the sstream)
}

/*
* Name: m_wasThereACompileError
* Purpose: Checks if there was an error during compiling the shaders
* Accepts: a Shader ID, a string by reference for the error
* Returns: false if the shader was compiled without a compile error
*/
bool ShaderManager::m_wasThereACompileError(unsigned int shaderID,
	std::string &errorText)
{
	errorText = "";	// No error

	GLint isCompiled = 0;
	glGetShaderiv(shaderID, GL_COMPILE_STATUS, &isCompiled);
	if (isCompiled == GL_FALSE)
	{
		GLint maxLength = 0;
		glGetShaderiv(shaderID, GL_INFO_LOG_LENGTH, &maxLength);

		char* pLogText = new char[maxLength];
		// Fill with zeros, maybe...?
		glGetShaderInfoLog(shaderID, maxLength, &maxLength, pLogText);
		// Copy char array to string
		errorText.append(pLogText);

		// Extra code that Michael forgot wasn't there... 
		this->m_lastError.append("\n");
		this->m_lastError.append(errorText);


		delete[] pLogText;	// Oops

		return true;	// There WAS an error
	}
	return false; // There WASN'T an error
}

/*
* Name: m_wasThereALinkError
* Purpose: Checks if there was an error during the linking of the shaders
* Accepts: a program ID, a string by reference for the error
* Returns: false if the shader was compiled without a linker error
*/
bool ShaderManager::m_wasThereALinkError(unsigned int programID, std::string &errorText)
{
	errorText = "";	// No error

	GLint wasError = 0;
	glGetProgramiv(programID, GL_LINK_STATUS, &wasError);
	if (wasError == GL_FALSE)
	{
		GLint maxLength = 0;
		glGetProgramiv(programID, GL_INFO_LOG_LENGTH, &maxLength);

		char* pLogText = new char[maxLength];
		// Fill with zeros, maybe...?
		glGetProgramInfoLog(programID, maxLength, &maxLength, pLogText);
		// Copy char array to string
		errorText.append(pLogText);

		// Extra code that Michael forgot wasn't there... 
		this->m_lastError.append("\n");
		this->m_lastError.append(errorText);


		delete[] pLogText;

		// There WAS an error
		return true;
	}

	// There WASN'T an error
	return false;
}

/*
* Name: getLastError
* Purpose: return the last possible shader error while also clearing the error
* Accepts: Nothing
* Returns: a string of the last error
*/
std::string ShaderManager::getLastError(void)
{
	std::string lastErrorTemp = this->m_lastError;
	this->m_lastError = "";
	return lastErrorTemp;
}


/*
* Name: m_compileShaderFromSource
* Purpose: Compiles the shader using information from a shader source
* Accepts: a Shader variable by reference, a string by reference for the error
* Returns: true if the shader was compiled
*/
bool ShaderManager::m_compileShaderFromSource(ShaderManager::Shader &shader, std::string &error)
{
	error = "";

	const unsigned int MAXLINESIZE = 8 * 1024;	// About 8K PER LINE, which seems excessive

	unsigned int numberOfLines = static_cast<unsigned int>(shader.vecSource.size());

	// This is an array of pointers to strings. aka the lines of source
	char** arraySource = new char*[numberOfLines];
	// Clear array to all zeros (i.e. '\0' or null terminator)
	memset(arraySource, 0, numberOfLines);

	for (unsigned int indexLine = 0; indexLine != numberOfLines; indexLine++)
	{
		unsigned int numCharacters = (unsigned int)shader.vecSource[indexLine].length();
		// Create an array of chars for each line
		arraySource[indexLine] = new char[numCharacters + 2];		// For the '\n' and '\0' at end
		memset(arraySource[indexLine], 0, numCharacters + 2);

		// Copy line of source into array
		for (unsigned int indexChar = 0; indexChar != shader.vecSource[indexLine].length(); indexChar++)
		{
			arraySource[indexLine][indexChar] = shader.vecSource[indexLine][indexChar];
		}//for ( unsigned int indexChar = 0...

		 // At a '\0' at end (just in case)
		arraySource[indexLine][numCharacters + 0] = '\n';
		arraySource[indexLine][numCharacters + 1] = '\0';

	}

	glShaderSource(shader.ID, numberOfLines, arraySource, NULL);
	glCompileShader(shader.ID);

	// Get rid of the temp source "c" style array
	for (unsigned int indexLine = 0; indexLine != numberOfLines; indexLine++)
	{	// Delete this line
		delete[] arraySource[indexLine];
	}
	// And delete the original char** array
	delete[] arraySource;

	// Did it work? 
	std::string errorText = "";
	if (this->m_wasThereACompileError(shader.ID, errorText))
	{
		std::stringstream ssError;
		ssError << shader.getShaderTypeString();
		ssError << " compile error: ";
		ssError << errorText;
		error = ssError.str();
		return false;
	}

	return true;
}


/*
* Name: createProgramFromFile
* Purpose: creates a shader program using a vertex and fragment shader
* Accepts: a name to reference the new program, a vertex shader
* Returns: true if the the program was created
*/
bool ShaderManager::createProgramFromFile(
	std::string friendlyName,
	Shader &vertexShad,
	Shader &fragShader)
{
	std::string errorText = "";


	// Shader loading happening before vertex buffer array
	vertexShad.ID = glCreateShader(GL_VERTEX_SHADER);
	vertexShad.shaderType = Shader::VERTEX_SHADER;
	//  char* vertex_shader_text = "wewherlkherlkh";
	// Load some text from a file...
	if (!this->m_loadSourceFromFile(vertexShad))
	{
		return false;
	}//if ( ! this->m_loadSourceFromFile(...

	errorText = "";
	if (!this->m_compileShaderFromSource(vertexShad, errorText))
	{
		this->m_lastError = errorText;
		return false;
	}//if ( this->m_compileShaderFromSource(...



	fragShader.ID = glCreateShader(GL_FRAGMENT_SHADER);
	fragShader.shaderType = Shader::FRAGMENT_SHADER;
	if (!this->m_loadSourceFromFile(fragShader))
	{
		return false;
	}//if ( ! this->m_loadSourceFromFile(...

	if (!this->m_compileShaderFromSource(fragShader, errorText))
	{
		this->m_lastError = errorText;
		return false;
	}//if ( this->m_compileShaderFromSource(...


	ShaderProgram curProgram;
	curProgram.ID = glCreateProgram();

	glAttachShader(curProgram.ID, vertexShad.ID);
	glAttachShader(curProgram.ID, fragShader.ID);
	glLinkProgram(curProgram.ID);

	// Was there a link error? 
	errorText = "";
	if (this->m_wasThereALinkError(curProgram.ID, errorText))
	{
		std::stringstream ssError;
		ssError << "Shader program link error: ";
		ssError << errorText;
		this->m_lastError = ssError.str();
		return false;
	}

	// At this point, shaders are compiled and linked into a program

	curProgram.friendlyName = friendlyName;

	// Add the shader to the map
	this->m_ID_to_Shader[curProgram.ID] = curProgram;
	// Save to other map, too
	this->m_name_to_ID[curProgram.friendlyName] = curProgram.ID;

	return true;
}
