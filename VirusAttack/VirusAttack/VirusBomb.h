#ifndef _VirusBomb_HG_
#define _VirusBomb_HG_
// Header Definition of VirusBomb
// Name: VirusBomb
// Purpose: The representation of what a "Virus Bomb" will be during gameplay in terms of behaviour and properties
//			Contains integer values that represent the properties of the explosion of the bomb and a method to start the
//			explosion
class VirusBomb
{
public:
	VirusBomb();
	~VirusBomb();

	int explosionRadius;
	int explosionDamage;

	/*
	* Name: DetonateBomb
	* Purpose: create an explosion in the game based on the radius and damage values
	* Accepts: Nothing
	* Returns: Nothing
	*/
	void DetonateBomb();
};

#endif // !_VirusBomb_HG_
