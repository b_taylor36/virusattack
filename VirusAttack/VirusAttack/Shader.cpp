#include "ShaderManager.h"

//Constructor
ShaderManager::Shader::Shader()
{
	this->ID = 0;
	this->shaderType = Shader::UNKNOWN;
	return;
}

//Destructor
ShaderManager::Shader::~Shader()
{
	return;
}
/*
* Name: getShaderTypeString
* Purpose: Converts the enumtype of the Shader in the form of a string
* Accepts: Nothing
* Returns: The String equivalent of the eShaderType of the Shader
*/
std::string ShaderManager::Shader::getShaderTypeString(void)
{
	switch (this->shaderType)
	{
	case Shader::VERTEX_SHADER:
		return "VERTEX_SHADER";
		break;
	case Shader::FRAGMENT_SHADER:
		return "FRAGMENT_SHADER";
		break;
	case Shader::UNKNOWN:
	default:
		return "UNKNOWN_SHADER_TYPE";
		break;
	}
	// Should never reach here...
	return "UNKNOWN_SHADER_TYPE";
}
