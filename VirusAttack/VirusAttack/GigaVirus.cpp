#include"GigaVirus.h"
#include"GameObject.h"
//Constructor
GigaVirus::GigaVirus()
{
	//TODO Set some defaults
	return;
}

//Destructor
GigaVirus::~GigaVirus()
{
	//Delete anything neccessary
	return;

}

//Pimpl implementation
class GigaVirus::ImplementationData
{
public:
	int internalData = 0;
};


//Method Implementations


void GigaVirus::FindTarget()
{
	return;
}

void GigaVirus::SpawnChild()
{
	return;
}

void GigaVirus::SpawnObjectOnDeath()
{
	return;
}

void GigaVirus::SetPosition(glm::vec3 newPos)
{
	this->pMesh->position = newPos;
	return;
}

void GigaVirus::SetVelocity(glm::vec3 newVel)
{
	this->pMesh->vel = newVel;
	return;
}

