#include "ModelUtilities.h" 


/*
* Name: ReadFileToToken
* Purpose: Reads file until the passed in token is found
* Accepts: an inputstream by reference, and the token string to stop reading the file at
* Returns: Nothing
*/
void ReadFileToToken(std::ifstream &file, std::string token)
{
	bool bKeepReading = true;
	std::string garbage;
	do
	{
		file >> garbage;		
		if (garbage == token)
		{
			return;
		}
	} while (bKeepReading);
	return;
}



/*
* Name: LoadPlyFileIntoMesh
* Purpose: reads information from a ply file and pushes that information into the passed in Mesh objet
* Accepts: the filename to read from, and the Mesh object to add information to
* Returns: true if succeced and false if it failed
*/
bool LoadPlyFileIntoMesh(std::string filename, Mesh &theMesh)
{
	// Load the vertices
	std::ifstream plyFile(filename.c_str());

	if (!plyFile.is_open())
	{	// Didn't open file, so return
		return false;
	}
	// File is open, let's read it

	ReadFileToToken(plyFile, "vertex");
	plyFile >> theMesh.numberOfVertices;

	ReadFileToToken(plyFile, "face");
	plyFile >> theMesh.numberOfTriangles;

	ReadFileToToken(plyFile, "end_header");

	// Allocate the appropriate sized array (+ a little bit)
	theMesh.pVertices = new VertexInfo[theMesh.numberOfVertices];
	theMesh.pTriangles = new glm::vec3[theMesh.numberOfTriangles];

	// Read vertices
	for (int index = 0; index < theMesh.numberOfVertices; index++)
	{		
		float x, y, z;

		plyFile >> x;
		plyFile >> y;
		plyFile >> z;

		theMesh.pVertices[index].pos = glm::vec3(x, y, z);
		theMesh.pVertices[index].col = glm::vec3(1.0f, 1.0f, 1.0f);
	}

	// Load the triangle (or face) information, too
	for (int count = 0; count < theMesh.numberOfTriangles; count++)
	{
		int discard = 0;
		plyFile >> discard;									
		plyFile >> theMesh.pTriangles[count].x;	
		plyFile >> theMesh.pTriangles[count].y;	
		plyFile >> theMesh.pTriangles[count].z;	
	}

	return true;
}

/*
* Name: LoadPlyFileIntoMeshWithNormals
* Purpose: reads information from a ply file including normals and pushes that information into the passed in Mesh objet
* Accepts: the filename to read from, and the Mesh object to add information to
* Returns: true if succeced and false if it failed
*/
bool LoadPlyFileIntoMeshWithNormals(std::string filename, Mesh &theMesh)
{
	// Load the vertices
	std::ifstream plyFile(filename.c_str());

	if (!plyFile.is_open())
	{	// Didn't open file, so return
		return false;
	}

	ReadFileToToken(plyFile, "vertex");
	plyFile >> theMesh.numberOfVertices;

	ReadFileToToken(plyFile, "face");
	plyFile >> theMesh.numberOfTriangles;

	ReadFileToToken(plyFile, "end_header");

	// Allocate the appropriate sized array (+ a little bit)
	theMesh.pVertices = new VertexInfo[theMesh.numberOfVertices];
	theMesh.pTriangles = new glm::vec3[theMesh.numberOfTriangles];

	// Read vertices
	for (int index = 0; index < theMesh.numberOfVertices; index++)
	{
	
		float x, y, z, nx, ny, nz, confidence, intensity;

		plyFile >> x;
		plyFile >> y;
		plyFile >> z;
		plyFile >> nx >> ny >> nz;

		theMesh.pVertices[index].pos.x = x;	
		theMesh.pVertices[index].pos.y = y;	
		theMesh.pVertices[index].pos.z = z;
		theMesh.pVertices[index].col.r = 1.0f;	
		theMesh.pVertices[index].col.g = 1.0f;	
		theMesh.pVertices[index].col.b = 1.0f;	
		theMesh.pVertices[index].npos.x = nx;	
		theMesh.pVertices[index].npos.y = ny;	
		theMesh.pVertices[index].npos.z = nz;	
	}

	// Load the triangle (or face) information, too
	for (int count = 0; count < theMesh.numberOfTriangles; count++)
	{
		int discard = 0;
		plyFile >> discard;									
		plyFile >> theMesh.pTriangles[count].x;	
		plyFile >> theMesh.pTriangles[count].y;	
		plyFile >> theMesh.pTriangles[count].z;	
	}

	
	return true;
}