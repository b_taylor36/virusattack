#ifndef _Physics_HG_
#define _Physics_HG_

#include<glm\glm.hpp>
//forward declares
class GameObject;
class Mesh;
class PhysTriangle;

/*
* Name: PenetrationTestSphereSphere
* Purpose: To check if 2 Spheres have penetrated
* Accepts: 2 game object pointers
* Returns: true if the spheres are penetrating
*/
bool PenetrationTestSphereSphere(GameObject* pA, GameObject* pB);

/*
* Name: PerformSphereSphereCollision
* Purpose: to perform a response to a sphere-sphere collision
* Accepts: 2 game object pointers
* Returns: Nothing
*/
void PerformSphereSphereCollision(GameObject* pA, GameObject* pB);

/*
* Name: FindClosestPointsOnTerrainTriangles
* Purpose: To check for the closest point between a gameobject and a terrains' triangles
* Accepts: a game object pointer, a Mesh to test against the game object, a vec3 point by reference and a Phystriangle by reference
* Returns: true if the object is penetrating one of the triangles
*/
bool FindClosestPointsOnTerrainTriangles(GameObject* pO, Mesh theMesh, glm::vec3 &point, PhysTriangle &triangle);

/*
* Name: PerformSphereTriangleCollision
* Purpose: to perform a response to a sphere-triangle collision
* Accepts: a game object pointer, a vec3 point, a PhysTriangle, a bool to determine is the object is a OBB
* Returns: Nothing
*/
void PerformSphereTriangleCollision(GameObject* pA, glm::vec3 point, PhysTriangle triangle, bool isOBB);

/*
* Name: CalculateVelocity
* Purpose: calculate the return velocity of an object during collision
* Accepts: a vec3 containing velocity, and a vec3 containing the difference in position between the two objects
* Returns: a vec3 containing the return velocity
*/
glm::vec3 CalculateVelocity(glm::vec3 otherObjectVelocity, glm::vec3 positionDiff);

#endif
