#ifndef _Mesh_HG_
#define _Mesh_HG_

#include "VertexInfo.h"
#include <string>
#include <glm/vec3.hpp>
#include<vector>
#include"PhysTriangle.h"
// Header Definition of Mesh
// Name: Mesh
// Purpose: Store Relevant information pertaining to Meshes that have been Loaded
class Mesh
{
public:
	//Constructor/Destructor
	Mesh();
	~Mesh();
	//the friendly name of the mesh
	std::string name;	

	//Stored information of every vertex					
	VertexInfo* pVertices;						
	//Total number of vertices found int pVertices
	int numberOfVertices;

	//Stored information for every triangle in the mesh
	glm::vec3* pTriangles;						
	//Total number of triangles in the mesh
	int numberOfTriangles;

	// Used for the physics calculations...Basically a copy of pTriangles
	std::vector< PhysTriangle > vecPhysTris;


	//Variables for bounding boxes
	glm::vec3 minXYZ;
	glm::vec3 maxXYZ;
	glm::vec3 maxExtentXYZ;
	float maxExtent;	

	//Used for scaling meshes
	float scaleForUnitBBox;

	/*
	* Name: GeneratePhysicsTriangels
	* Purpose: Creates a copy of pTriangles and pushes the information into vecPhysTris so that it can be used for physics
	*		   Calculations
	* Accepts: Nothing
	* Returns: Nothing
	*/
	void GeneratePhysicsTriangles(void);

	/*
	* Name: CalculateExtents
	* Purpose: Finds the extents of the mesh using pVertices to compare each vertex XYZ to every other vertex XYZ to store
	*	       the extents into the above variables
	* Accepts: Nothing
	* Returns: Nothing
	*/
	void CalculateExtents(void);

	/*
	* Name: CalculateNormals
	* Purpose: Finds the normals of the mesh using pVertices and normalizing each vertex
	* Accepts: Nothing
	* Returns: Nothing
	*/
	void CalculateNormals(void);
};


#endif
