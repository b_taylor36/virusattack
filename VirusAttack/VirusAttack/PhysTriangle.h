#ifndef _PHYSTRIANGLE_HG_
#define _PHYSTRIANGLE_HG_

#include<glm/vec3.hpp>

// Header Definition of PhysTriangle
// Name: PhysTriangle
// Purpose: Store Relevant information pertaining to the Triangles in the Mesh that will be used for Physics Calculation.
//			Will also hold relevant functions for triangle based physics
class PhysTriangle
{
public:
	
	//A Triangle
	glm::vec3 vertex[3];

	/*
	* Name: ClosestPtPointTriangle
	* Purpose: Find the closest point in the passed in triangle XYZ coordiantes with a passed in point Coordiante
	* Accepts: the point to test against, the first coordinate of the triangle, the second coordinate of the triangle,
	*		   the third coordinate of the triangle
	* Returns: the calculated closest point XYZ between the point and triangle 
	*/
	glm::vec3 ClosestPtPointTriangle(glm::vec3 p, glm::vec3 a,
		glm::vec3 b, glm::vec3 c);


	/*
	* Name: ClosestPtPointTriangle
	* Purpose: Returns the closest point from the point passed in, to the working triangle (vertex[3])
	* Accepts: a point coordiante (XYZ) to test the triangle against
	* Returns: a point corrdiante (XYZ) of the triangle that is the closest to the passed in point
	*/
	glm::vec3 ClosestPtPointTriangle(glm::vec3 p);
};
#endif // !_PHYSTRIANGLE_HG_