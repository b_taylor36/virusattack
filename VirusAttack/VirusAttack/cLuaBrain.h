#ifndef _cLuaBrain_HG_
#define _cLuaBrain_HG_

extern "C" {
#include <Lua5.3.3/lua.h>
#include <Lua5.3.3/lua.h>
#include <Lua5.3.3/lauxlib.h>
#include <Lua5.3.3/lualib.h>
}

#include <string>
#include <vector>
//#include "cGameObject.h"
#include <map>

// Forward declaration for cyclical reference
class cGameObject;

class cLuaBrain
{
public:
	// Init Lua and set callback functions
	cLuaBrain();
	~cLuaBrain();
	void LoadScript(std::string scriptName,
		std::string scriptSource);
	void DeleteScript(std::string scriptName);

	static int PlayAudio(lua_State *L);
	
	void RunScriptImmediately(std::string script);

std::map< std::string /*scriptName*/,
		std::string /*scriptSource*/ > m_mapScripts;
private:
	

	lua_State* m_pLuaState;

	std::string m_decodeLuaErrorToString(int error);
};

#endif
