#ifndef _GigaVirus_HG_
#define _GigaVirus_HG_

#include "iVirus.h"
#include"VirusBomb.h"
class GameObject;	// forward declare

					//inherit interface
// Header Definition of GigaVirus
// Name: GigaVirus
// Purpose: The representation of what a "Giga Virus" will be during gameplay in terms of behaviour and properties
//			Contains inherited methods from the iVirus interface that influences what it can do
class GigaVirus : public iVirus
{
public:
	GigaVirus();
	virtual ~GigaVirus();

	//Inherited methods
	virtual void FindTarget(void);
	virtual void SpawnChild(void);
	virtual void SpawnObjectOnDeath(void);

	virtual void SetPosition(glm::vec3 newPos);
	virtual void SetVelocity(glm::vec3 newVel);

//	PowerUp* powerUp;
	VirusBomb* virusBomb;
	GameObject* pMesh;

private:	//Pimpl
	class ImplementationData;
	ImplementationData* pimpl;
};

#endif 
