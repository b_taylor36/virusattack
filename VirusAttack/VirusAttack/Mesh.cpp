#include "Mesh.h"
#include <glm/glm.hpp>		

//Constructor
Mesh::Mesh()
{
	this->numberOfVertices = 0;
	this->numberOfTriangles = 0;
	this->maxExtent = 0.0f;
	this->scaleForUnitBBox = 1.0f;
	return;
}

//Destructor
Mesh::~Mesh()
{

	return;
}

/*
* Name: CalculateExtents
* Purpose: Finds the extents of the mesh using pVertices to compare each vertex XYZ to every other vertex XYZ to store
*	       the extents into the appropriate extent variables
* Accepts: Nothing
* Returns: Nothing
*/
void Mesh::CalculateExtents(void)
{
	// Assume 1st vertex is both max and min
	this->minXYZ = this->pVertices[0].pos;
	this->maxXYZ = this->pVertices[0].pos;

	for (int index = 0; index != this->numberOfVertices; index++)
	{
		// x...
		if (this->pVertices[index].pos.x < this->minXYZ.x)
		{
			this->minXYZ.x = this->pVertices[index].pos.x;
		}
		if (this->pVertices[index].pos.x > this->maxXYZ.x)
		{
			this->maxXYZ.x = this->pVertices[index].pos.x;
		}
		// y...
		if (this->pVertices[index].pos.y < this->minXYZ.y)
		{
			this->minXYZ.y = this->pVertices[index].pos.y;
		}
		if (this->pVertices[index].pos.y > this->maxXYZ.y)
		{
			this->maxXYZ.y = this->pVertices[index].pos.y;
		}
		// z...
		if (this->pVertices[index].pos.z < this->minXYZ.z)
		{
			this->minXYZ.z = this->pVertices[index].pos.z;
		}
		if (this->pVertices[index].pos.z > this->maxXYZ.z)
		{
			this->maxXYZ.z = this->pVertices[index].pos.z;
		}

	}

	this->maxExtentXYZ.x = this->maxXYZ.x - this->minXYZ.x;
	this->maxExtentXYZ.y = this->maxXYZ.y - this->minXYZ.y;
	this->maxExtentXYZ.z = this->maxXYZ.z - this->minXYZ.z;

	this->maxExtent = this->maxExtentXYZ.x;
	if (this->maxExtent < this->maxExtentXYZ.y)
	{	// Y is bigger
		this->maxExtent = this->maxExtentXYZ.y;
	}
	if (this->maxExtent < this->maxExtentXYZ.z)
	{	// Z is bigger
		this->maxExtent = this->maxExtentXYZ.z;
	}
	//
	this->scaleForUnitBBox = 1.0f / this->maxExtent;

	return;
}

/*
* Name: GeneratePhysicsTriangels
* Purpose: Creates a copy of pTriangles and pushes the information into vecPhysTris so that it can be used for physics
*		   Calculations
* Accepts: Nothing
* Returns: Nothing
*/
void Mesh::GeneratePhysicsTriangles(void)
{

	for (int triIndex = 0; triIndex < this->numberOfTriangles; triIndex++)
	{
		
		int triVert0_index = this->pTriangles[triIndex].x;
		int triVert1_index = this->pTriangles[triIndex].y;
		int triVert2_index = this->pTriangles[triIndex].z;


		PhysTriangle tempTri;

	

		VertexInfo V0 = this->pVertices[triVert0_index];
		tempTri.vertex[0].x = V0.pos.x;
		tempTri.vertex[0].y = V0.pos.y;
		tempTri.vertex[0].z = V0.pos.z;

		VertexInfo  V1 = this->pVertices[triVert1_index];
		tempTri.vertex[1].x = V1.pos.x;
		tempTri.vertex[1].y = V1.pos.y;
		tempTri.vertex[1].z = V1.pos.z;

		VertexInfo V2 = this->pVertices[triVert2_index];
		tempTri.vertex[2].x = V2.pos.x;
		tempTri.vertex[2].y = V2.pos.y;
		tempTri.vertex[2].z = V2.pos.z;

		this->vecPhysTris.push_back(tempTri);

	}
}

/*
* Name: CalculateNormals
* Purpose: Finds the normals of the mesh using pVertices and normalizing each vertex
* Accepts: Nothing
* Returns: Nothing
*/
void Mesh::CalculateNormals(void)
{
	// Go through each triangle...
	// ...calculate normal per vertex (3 vertices)
	// ...ADD this normal to the corresponding vertex

	// 2nd pass, 
	// go through all the vertices and "normalize" them
	for (int vertIndex = 0; vertIndex != this->numberOfVertices; vertIndex++)
	{
		this->pVertices[vertIndex].npos = glm::vec3(0.0f, 0.0f, 0.0f);

		/*this->pVertices[vertIndex].nx = 0.0f;
		this->pVertices[vertIndex].ny = 0.0f;
		this->pVertices[vertIndex].nz = 0.0f;*/
	}

	for (int triIndex = 0; triIndex != this->numberOfTriangles; triIndex++)
	{
		//cTriangle curTri = this->pTriangles[triIndex];
		glm::vec3 curTri = this->pTriangles[triIndex];
		// Calculate normal for each vertex
		glm::vec3 vertA = this->pVertices[(int)curTri.x].pos;
		glm::vec3 vertB = this->pVertices[(int)curTri.y].pos;
		glm::vec3 vertC = this->pVertices[(int)curTri.z].pos;
		/*glm::vec3 vertB = glm::vec3( this->pVertices[(int)curTri.vertex_ID_1 ].x,
		this->pVertices[(int)curTri.vertex_ID_1 ].y,
		this->pVertices[ curTri.vertex_ID_1 ].z );

		glm::vec3 vertC = glm::vec3( this->pVertices[ curTri.vertex_ID_2 ].x,
		this->pVertices[ curTri.vertex_ID_2 ].y,
		this->pVertices[ curTri.vertex_ID_2 ].z );*/

		// Cross of A-B and A-C (normal at vertex A)
		glm::vec3 normVec0 = glm::normalize(glm::cross(vertB - vertA, vertC - vertA));

		// Cross of B-A and B-C (normal at vertex B)
		glm::vec3 normVec1 = glm::normalize(glm::cross(vertA - vertB, vertC - vertB));

		// Cross of C-A and C-B (normal at vertex C)
		glm::vec3 normVec2 = glm::normalize(glm::cross(vertA - vertC, vertB - vertC));

		// Add the values to the current normals (vert A)
		this->pVertices[(int)curTri.x].npos.x += normVec0.x;
		this->pVertices[(int)curTri.x].npos.y += normVec0.y;
		this->pVertices[(int)curTri.x].npos.z += normVec0.z;

		// Add the values to the current normals (vert B)
		this->pVertices[(int)curTri.y].npos.x += normVec1.x;
		this->pVertices[(int)curTri.y].npos.y += normVec1.y;
		this->pVertices[(int)curTri.y].npos.z += normVec1.z;

		// Add the values to the current normals (vert C)
		this->pVertices[(int)curTri.z].npos.x += normVec2.x;
		this->pVertices[(int)curTri.z].npos.y += normVec2.y;
		this->pVertices[(int)curTri.z].npos.z += normVec2.z;
	}//

	 // 2nd pass: normalize the normals
	for (int vertIndex = 0; vertIndex != this->numberOfVertices; vertIndex++)
	{
		glm::vec3 norm = glm::vec3(this->pVertices[vertIndex].npos.x,
			this->pVertices[vertIndex].npos.y,
			this->pVertices[vertIndex].npos.z);
		// It's value DIV length
		glm::normalize(norm);
		// divide the xyz by the length of the vector

		this->pVertices[vertIndex].npos.x = norm.x;
		this->pVertices[vertIndex].npos.y = norm.y;
		this->pVertices[vertIndex].npos.z = norm.z;
	}

	return;
}